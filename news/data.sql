INSERT INTO nm.author (id, name, surname) OVERRIDING SYSTEM VALUE VALUES (1, 'Mikita', 'Bacharenka');
INSERT INTO nm.author (id, name, surname) OVERRIDING SYSTEM VALUE VALUES (2, 'Alex', 'Popov');
INSERT INTO nm.author (id, name, surname) OVERRIDING SYSTEM VALUE VALUES (3, 'Ivan', 'Titov');
INSERT INTO nm.author (id, name, surname) OVERRIDING SYSTEM VALUE VALUES (4, 'Semen', 'Bolshakov');
INSERT INTO nm.author (id, name, surname) OVERRIDING SYSTEM VALUE VALUES (5, 'Leonid', 'Spevakov');
INSERT INTO nm.author (id, name, surname) OVERRIDING SYSTEM VALUE VALUES (6, 'Fransua', 'Degterev');


--
-- TOC entry 2836 (class 0 OID 16398)
-- Dependencies: 197
-- Data for Name: news; Type: TABLE DATA; Schema: nm; Owner: postgres
--

INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (1, 'Man died after coala broke his', 'How could this happen? Lets find out!', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.', '2020-01-31', '2020-01-31');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (2, 'Title 2', 'Short text 2', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.', '2020-01-28', '2020-01-29');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (3, 'Title 3', 'Short text 3', 'Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles.', '2020-01-14', '2020-01-16');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (4, 'Man died after coala broke his', 'How could this happen? Lets find out!', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.', '2019-06-05', '2018-01-12');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (5, 'Man died after coala broke his', 'How could this happen? Lets find out!', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.', '2019-06-05', '2018-01-12');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (7, 'Man died after coala broke his', 'How could this happen? Lets find out!', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.', '2019-06-05', '2018-01-12');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (8, 'Man died after coala broke his', 'How could this happen? Lets find out!', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.', '2019-06-05', '2018-01-12');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (10, 'Title again', 'Short text again', 'Full text again', '2020-01-28', '2020-01-29');
INSERT INTO nm.news (id, title, short_text, full_text, creation_date, modification_date) OVERRIDING SYSTEM VALUE VALUES (9, 'Title 9', 'Short text 9', 'Full text 9', '2005-03-18', '2005-03-18');


--
-- TOC entry 2840 (class 0 OID 16415)
-- Dependencies: 201
-- Data for Name: news_author; Type: TABLE DATA; Schema: nm; Owner: postgres
--

INSERT INTO nm.news_author (news_id, author_id) VALUES (1, 1);
INSERT INTO nm.news_author (news_id, author_id) VALUES (2, 2);
INSERT INTO nm.news_author (news_id, author_id) VALUES (3, 3);
INSERT INTO nm.news_author (news_id, author_id) VALUES (7, 2);
INSERT INTO nm.news_author (news_id, author_id) VALUES (8, 4);
INSERT INTO nm.news_author (news_id, author_id) VALUES (10, 4);
INSERT INTO nm.news_author (news_id, author_id) VALUES (9, 6);
INSERT INTO nm.news_author (news_id, author_id) VALUES (4, 1);
INSERT INTO nm.news_author (news_id, author_id) VALUES (5, 1);


--
-- TOC entry 2843 (class 0 OID 16435)
-- Dependencies: 204
-- Data for Name: news_tag; Type: TABLE DATA; Schema: nm; Owner: postgres
--

INSERT INTO nm.news_tag (news_id, tag_id) VALUES (2, 3);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (5, 3);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (5, 4);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (7, 4);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (8, 3);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (3, 4);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (4, 4);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (4, 2);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (4, 3);
INSERT INTO nm.news_tag (news_id, tag_id) VALUES (4, 6);


--
-- TOC entry 2845 (class 0 OID 16453)
-- Dependencies: 206
-- Data for Name: roles; Type: TABLE DATA; Schema: nm; Owner: postgres
--



--
-- TOC entry 2841 (class 0 OID 16428)
-- Dependencies: 202
-- Data for Name: tag; Type: TABLE DATA; Schema: nm; Owner: postgres
--

INSERT INTO nm.tag (id, name) OVERRIDING SYSTEM VALUE VALUES (1, 'beauty');
INSERT INTO nm.tag (id, name) OVERRIDING SYSTEM VALUE VALUES (2, 'nature');
INSERT INTO nm.tag (id, name) OVERRIDING SYSTEM VALUE VALUES (3, 'society');
INSERT INTO nm.tag (id, name) OVERRIDING SYSTEM VALUE VALUES (4, 'science');
INSERT INTO nm.tag (id, name) OVERRIDING SYSTEM VALUE VALUES (6, 'culture');
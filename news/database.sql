--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11
-- Dumped by pg_dump version 10.11

-- Started on 2020-02-07 18:47:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2851 (class 1262 OID 16393)
-- Name: news_management; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE news_management WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE news_management OWNER TO postgres;

\connect news_management

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 16396)
-- Name: nm; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA nm;


ALTER SCHEMA nm OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2854 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16408)
-- Name: author; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm.author (
    id bigint NOT NULL,
    name character varying(30) NOT NULL,
    surname character varying(30) NOT NULL
);


ALTER TABLE nm.author OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16413)
-- Name: author_id_seq; Type: SEQUENCE; Schema: nm; Owner: postgres
--

ALTER TABLE nm.author ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME nm.author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 197 (class 1259 OID 16398)
-- Name: news; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm.news (
    id bigint NOT NULL,
    title character varying(30) NOT NULL,
    short_text character varying(100) NOT NULL,
    full_text character varying(2000) NOT NULL,
    creation_date date NOT NULL,
    modification_date date NOT NULL
);


ALTER TABLE nm.news OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16415)
-- Name: news_author; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm.news_author (
    news_id bigint NOT NULL,
    author_id bigint NOT NULL
);


ALTER TABLE nm.news_author OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16406)
-- Name: news_id_seq; Type: SEQUENCE; Schema: nm; Owner: postgres
--

ALTER TABLE nm.news ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME nm.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 204 (class 1259 OID 16435)
-- Name: news_tag; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm.news_tag (
    news_id bigint NOT NULL,
    tag_id bigint NOT NULL
);


ALTER TABLE nm.news_tag OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16453)
-- Name: roles; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm.roles (
    user_id bigint NOT NULL,
    role_name character varying(30) NOT NULL
);


ALTER TABLE nm.roles OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16428)
-- Name: tag; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm.tag (
    id bigint NOT NULL,
    name character varying(30) NOT NULL
);


ALTER TABLE nm.tag OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16433)
-- Name: tag_id_seq; Type: SEQUENCE; Schema: nm; Owner: postgres
--

ALTER TABLE nm.tag ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME nm.tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 16448)
-- Name: user; Type: TABLE; Schema: nm; Owner: postgres
--

CREATE TABLE nm."user" (
    id bigint NOT NULL,
    name character varying(20) NOT NULL,
    surname character varying(20) NOT NULL,
    login character varying(30) NOT NULL,
    password character varying(30) NOT NULL
);


ALTER TABLE nm."user" OWNER TO postgres;



--
-- TOC entry 2862 (class 0 OID 0)
-- Dependencies: 200
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: nm; Owner: postgres
--

SELECT pg_catalog.setval('nm.author_id_seq', 6, true);


--
-- TOC entry 2863 (class 0 OID 0)
-- Dependencies: 198
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: nm; Owner: postgres
--

SELECT pg_catalog.setval('nm.news_id_seq', 10, true);


--
-- TOC entry 2864 (class 0 OID 0)
-- Dependencies: 203
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: nm; Owner: postgres
--

SELECT pg_catalog.setval('nm.tag_id_seq', 6, true);


--
-- TOC entry 2705 (class 2606 OID 16412)
-- Name: author author_pkey; Type: CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- TOC entry 2703 (class 2606 OID 16405)
-- Name: news news_pkey; Type: CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- TOC entry 2707 (class 2606 OID 16432)
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- TOC entry 2709 (class 2606 OID 16452)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2711 (class 2606 OID 16423)
-- Name: news_author author_id; Type: FK CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.news_author
    ADD CONSTRAINT author_id FOREIGN KEY (author_id) REFERENCES nm.author(id);


--
-- TOC entry 2710 (class 2606 OID 16418)
-- Name: news_author news_id; Type: FK CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.news_author
    ADD CONSTRAINT news_id FOREIGN KEY (news_id) REFERENCES nm.news(id);


--
-- TOC entry 2712 (class 2606 OID 16438)
-- Name: news_tag news_id; Type: FK CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.news_tag
    ADD CONSTRAINT news_id FOREIGN KEY (news_id) REFERENCES nm.news(id) NOT VALID;


--
-- TOC entry 2713 (class 2606 OID 16443)
-- Name: news_tag tag_id; Type: FK CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.news_tag
    ADD CONSTRAINT tag_id FOREIGN KEY (tag_id) REFERENCES nm.tag(id) NOT VALID;


--
-- TOC entry 2714 (class 2606 OID 16456)
-- Name: roles user_id; Type: FK CONSTRAINT; Schema: nm; Owner: postgres
--

ALTER TABLE ONLY nm.roles
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES nm."user"(id);


--
-- TOC entry 2852 (class 0 OID 0)
-- Dependencies: 8
-- Name: SCHEMA nm; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA nm TO nm_user;


--
-- TOC entry 2855 (class 0 OID 0)
-- Dependencies: 199
-- Name: TABLE author; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm.author TO nm_user;


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 197
-- Name: TABLE news; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm.news TO nm_user;


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE news_author; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm.news_author TO nm_user;


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 204
-- Name: TABLE news_tag; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm.news_tag TO nm_user;


--
-- TOC entry 2859 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE roles; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm.roles TO nm_user;


--
-- TOC entry 2860 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE tag; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm.tag TO nm_user;


--
-- TOC entry 2861 (class 0 OID 0)
-- Dependencies: 205
-- Name: TABLE "user"; Type: ACL; Schema: nm; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE nm."user" TO nm_user;


--
-- TOC entry 1700 (class 826 OID 16397)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: nm; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA nm REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA nm GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES  TO nm_user;


--
-- TOC entry 1699 (class 826 OID 16395)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: -; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES  TO nm_user;


-- Completed on 2020-02-07 18:47:43

--
-- PostgreSQL database dump complete
--


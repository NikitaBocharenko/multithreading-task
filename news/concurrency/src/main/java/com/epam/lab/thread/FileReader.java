package com.epam.lab.thread;

import com.epam.lab.processing.ObjectCreator;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Set;
import java.util.stream.Stream;

public class FileReader implements Runnable {
    private final Path rootPath;
    private final Path errorPath;
    private final ObjectCreator objectCreator;
    private final Set<Path> currentProcessingPaths;

    private static final Logger LOG = Logger.getLogger(FileReader.class);

    public FileReader(Path rootPath, Path errorPath, ObjectCreator objectCreator, Set<Path> currentProcessingPaths) {
        this.rootPath = rootPath;
        this.errorPath = errorPath;
        this.objectCreator = objectCreator;
        this.currentProcessingPaths = currentProcessingPaths;
    }

    @Override
    public void run() {
        LOG.debug("File reader started");
        try (Stream<Path> walk = Files.walk(rootPath)) {
            walk
                    .filter(path -> !path.startsWith(errorPath))
                    .filter(path -> Files.isRegularFile(path))
                    .filter(currentProcessingPaths::add)
                    .forEach(this::processFile);
        } catch (IOException e) {
            LOG.error("IO exception in file walking", e);
        }
        LOG.debug("File reader stopped");
    }

    private void processFile(Path path) {
        if (Files.isRegularFile(path)) {
            LOG.debug(Thread.currentThread().getId() + ": try to lock " + path.getFileName());
            try (FileChannel channel = FileChannel.open(path, StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.DELETE_ON_CLOSE);
                 FileLock lock = channel.lock()) {
                String fileContent = readFileContent(channel);
                LOG.debug(Thread.currentThread().getId() + ": content: " + fileContent.substring(0, 20) + "...");
                if (objectCreator.create(fileContent)) {
                    LOG.debug(Thread.currentThread().getId() + ": file " + path.getFileName() + " is valid");
                } else {
                    LOG.debug(Thread.currentThread().getId() + ": file " + path.getFileName() + " contains error json");
                    Files.write(errorPath.resolve(path.getFileName()), fileContent.getBytes());
                }
            } catch (IOException e) {
                LOG.error(Thread.currentThread().getId() + ": IO exception", e);
            } catch (OverlappingFileLockException e) {
                LOG.debug(Thread.currentThread().getId() + ": already locked " + path.getFileName());
            }
        }
        currentProcessingPaths.remove(path);
    }

    private String readFileContent(FileChannel channel) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        while (channel.read(buffer) > 0) {
            out.write(buffer.array(), 0, buffer.position());
            buffer.clear();
        }
        return new String(out.toByteArray(), StandardCharsets.UTF_8);
    }
}

package com.epam.lab.thread;

import com.epam.lab.processing.ObjectCreator;
import com.epam.lab.processing.ObjectSaverImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class ThreadManager implements Runnable {
    private final ObjectCreator objectCreator;
    private final ObjectSaverImpl objectSaver;
    private final Set<Path> currentProcessingPaths = ConcurrentHashMap.newKeySet();

    public ThreadManager(ObjectCreator objectCreator, ObjectSaverImpl objectSaver) {
        this.objectCreator = objectCreator;
        this.objectSaver = objectSaver;
    }

    private static final String ERROR_FOLDER_NAME = "error";
    private static final Logger LOG = Logger.getLogger(ThreadManager.class);

    @Value("${root-folder}")
    private String rootPathStr;

    @Value("${scan-delay}")
    private Long scanDelay;

    @Value("${thread-count}")
    private Integer threadCount;

    @EventListener
    public void handleContextRefreshEvent(ContextRefreshedEvent ctxRefreshEvt) {
        LOG.info("Starting thread manager...");
        Thread fileReadersManagerThread = new Thread(this);
        fileReadersManagerThread.setDaemon(true);
        fileReadersManagerThread.start();
    }

    @Override
    public void run() {
        LOG.info("File readers thread manager started");
        LOG.info("Thread count: " + threadCount);
        LOG.info("Scan delay: " + scanDelay);
        Path rootPath = Paths.get(rootPathStr);
        LOG.info("Root path: " + rootPath);
        if (!Files.isDirectory(rootPath)) {
            LOG.error("There is no JSON folder!");
            return;
        }
        Path errorPath = createErrorPath(rootPath);
        LOG.info("Error path: " + errorPath);
        startObjectSaverThread();
        while (true) {
            sleepForScanDelay();
            ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
            for (int i = 0; i < threadCount; i++) {
                executorService.execute(new FileReader(rootPath, errorPath, objectCreator, currentProcessingPaths));
            }
            LOG.debug("New file readers started");
            executorService.shutdown();
            boolean allTasksDone = awaitTermination(executorService);
            if (allTasksDone) {
                LOG.debug("All file readers successfully finish their tasks");
            } else {
                LOG.info("File reader haven't enough time to finish their tasks");
            }
        }
    }

    private Path createErrorPath(Path rootPath) {
        Path errorPath = rootPath.resolve(ERROR_FOLDER_NAME);
        if (!Files.isDirectory(errorPath)) {
            try {
                Files.createDirectory(errorPath);
            } catch (IOException e) {
                LOG.error("Exception in error folder creation", e);
            }
        }
        return errorPath;
    }

    private void startObjectSaverThread() {
        Thread objectSaverThread = new Thread(objectSaver);
        objectSaverThread.setDaemon(true);
        objectSaverThread.start();
    }

    private void sleepForScanDelay() {
        try {
            Thread.sleep(scanDelay);
        } catch (InterruptedException e) {
            LOG.error("Exception while thread sleeping", e);
        }
    }

    private boolean awaitTermination(ExecutorService executorService) {
        boolean done = false;
        try {
            done = executorService.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            LOG.error("Interrupted exception in termination awaiting");
        }
        return done;
    }
}

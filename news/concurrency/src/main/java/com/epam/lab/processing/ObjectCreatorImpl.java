package com.epam.lab.processing;

import com.epam.lab.dto.NewsDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ObjectCreatorImpl implements ObjectCreator {
    private final ObjectMapper objectMapper;
    private final ObjectValidator objectValidator;
    private final ObjectSaver objectSaver;

    private static final Logger LOG = Logger.getLogger(ObjectCreatorImpl.class);

    public ObjectCreatorImpl(ObjectMapper objectMapper, ObjectValidator objectValidator, ObjectSaver objectSaver) {
        this.objectMapper = objectMapper;
        this.objectValidator = objectValidator;
        this.objectSaver = objectSaver;
    }

    @Override
    public boolean create(String jsonString) {
        List<NewsDto> news;
        try {
            news = objectMapper.readValue(jsonString, new TypeReference<List<NewsDto>>() {});
        } catch (JsonProcessingException e) {
            LOG.debug("JSON parsing exception");
            return false;
        }
        if (!objectValidator.validate(news)) {
            LOG.debug("JSON validation violation");
            return false;
        }
        objectSaver.saveObjects(news);
        return true;
    }
}

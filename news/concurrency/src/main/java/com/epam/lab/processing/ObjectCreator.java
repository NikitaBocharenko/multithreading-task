package com.epam.lab.processing;

public interface ObjectCreator {
    boolean create(String jsonString);
}

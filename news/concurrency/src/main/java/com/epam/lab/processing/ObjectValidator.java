package com.epam.lab.processing;

import com.epam.lab.dto.NewsDto;

import java.util.List;

public interface ObjectValidator {
    boolean validate(List<NewsDto> news);
}

package com.epam.lab.processing;

import com.epam.lab.dto.NewsDto;
import com.epam.lab.validation.AddingValidation;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Component
public class ObjectValidatorImpl implements ObjectValidator {
    private final Validator validator;

    public ObjectValidatorImpl(Validator validator) {
        this.validator = validator;
    }

    @Override
    public boolean validate(List<NewsDto> newsList) {
        for (NewsDto news : newsList) {
            Set<ConstraintViolation<NewsDto>> constraintViolations = validator.validate(news, AddingValidation.class);
            if (!constraintViolations.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}

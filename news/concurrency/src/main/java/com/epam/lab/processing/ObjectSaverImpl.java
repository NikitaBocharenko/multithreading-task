package com.epam.lab.processing;

import com.epam.lab.dto.NewsDto;
import com.epam.lab.service.NewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Component
public class ObjectSaverImpl implements ObjectSaver, Runnable {
    private final BlockingQueue<NewsDto> newsQueue;

    @Value("${queue-to-save.threshold}")
    private Integer queueThreshold;

    @Value("${queue-to-save.wait-interval}")
    private Long waitInterval;

    private static final Integer LOOP_WITHOUT_QUEUE_POOLING_MAX_NUMBER = 9;

    private final NewsService newsService;

    private static final Logger LOG = Logger.getLogger(ObjectSaverImpl.class);

    public ObjectSaverImpl(@Value("${queue-to-save.capacity}") Integer queueMaxCapacity, NewsService newsService) {
        newsQueue = new LinkedBlockingQueue<>(queueMaxCapacity);
        this.newsService = newsService;
    }

    @Override
    public void saveObjects(List<NewsDto> newsList) {
        newsList.forEach(news -> {
            try {
                newsQueue.put(news);
            } catch (InterruptedException e) {
                LOG.error("Interrupted exception in adding to the queue", e);
            }
        });
    }

    @Override
    public void run() {
        int loopWithoutQueuePollingCounter = 0;
        while(true) {
            if (newsQueue.size() > queueThreshold || loopWithoutQueuePollingCounter > LOOP_WITHOUT_QUEUE_POOLING_MAX_NUMBER) {
                loopWithoutQueuePollingCounter = 0;
                List<NewsDto> news = new ArrayList<>(queueThreshold);
                newsQueue.drainTo(news, queueThreshold);
                if (!news.isEmpty()) {
                    LOG.debug("News are removed from queue");
                    newsService.addAll(news);
                } else {
                    LOG.debug("Queue is empty");
                }
            } else {
                loopWithoutQueuePollingCounter++;
                LOG.debug("There is not enough news in queue, sleeping...");
                try {
                    Thread.sleep(waitInterval);
                } catch (InterruptedException e) {
                    LOG.error("Interrupted exception while waiting for more news in queue", e);
                }
            }
        }
    }
}

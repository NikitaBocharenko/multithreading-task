package com.epam.lab.processing;

import com.epam.lab.dto.NewsDto;

import java.util.List;

public interface ObjectSaver {
    void saveObjects(List<NewsDto> news);
}

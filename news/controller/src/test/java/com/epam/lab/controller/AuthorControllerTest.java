package com.epam.lab.controller;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.dto.AuthorDto;
import com.epam.lab.service.AuthorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class AuthorControllerTest {
    private AuthorService mockAuthorService;
    private AuthorController authorController;
    private CriteriaBuilder mockCriteriaBuilder;
    private List<AuthorDto> expectedAuthorList;
    private AuthorDto expectedAuthor;

    @Before
    public void setUpAuthorControllerTest() {
        prepareExpectedAuthor();
        prepareExpectedAuthorList();
        prepareMockAuthorService();
        prepareMockCriteriaBuilder();
        prepareAuthorController();
    }

    @Test
    public void shouldFindAuthors() {
        String[] searchConditions = {"one", "two", "three"};
        String[] sortConditions = {"four", "five", "six"};
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        List<AuthorDto> actualAuthors = authorController.find(searchConditions, sortConditions, 20, 30, mockResponse);
        assertEquals(expectedAuthorList, actualAuthors);
        verify(mockResponse, times(1)).addHeader("TotalEntityCount", "3");
    }

    @Test
    public void shouldFindAuthorById() {
        AuthorDto actualAuthor = authorController.find(1L);
        assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    public void shouldAddAuthor() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        authorController.add(expectedAuthor, mockRequest, mockResponse);
        verify(mockAuthorService, times(1)).add(expectedAuthor);
    }

    @Test
    public void shouldEditAuthor() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        authorController.edit(expectedAuthor, mockRequest, mockResponse);
        verify(mockAuthorService, times(1)).edit(expectedAuthor);
    }

    @Test
    public void shouldDeleteAuthor() {
        authorController.delete(1L);
        verify(mockAuthorService, times(1)).delete(1L);
    }

    private void prepareExpectedAuthor() {
        expectedAuthor = new AuthorDto(1L, "TestName1", "TestSurname1");
    }

    private void prepareExpectedAuthorList() {
        expectedAuthorList = new ArrayList<>();
        expectedAuthorList.add(expectedAuthor);
        expectedAuthorList.add(new AuthorDto(2L, "TestName2", "TestSurname2"));
        expectedAuthorList.add(new AuthorDto(3L, "TestName3", "TestSurname3"));
    }

    private void prepareMockAuthorService() {
        mockAuthorService = mock(AuthorService.class);
        when(mockAuthorService.get(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(expectedAuthorList);
        when(mockAuthorService.get(anyLong())).thenReturn(expectedAuthor);
        when(mockAuthorService.getQuantity(any(List.class))).thenReturn((long) expectedAuthorList.size());
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.buildSearchCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
        when(mockCriteriaBuilder.buildSortCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
    }

    private void prepareAuthorController() {
        authorController = new AuthorController(mockAuthorService, mockCriteriaBuilder);
    }
}

package com.epam.lab.controller;

import com.epam.lab.builder.CriteriaBuilder;
import com.epam.lab.dto.TagDto;
import com.epam.lab.service.TagService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class TagControllerTest {
    private TagService mockTagService;
    private CriteriaBuilder mockCriteriaBuilder;
    private TagController tagController;
    private List<TagDto> expectedTagList;
    private TagDto expectedTag;

    @Before
    public void setUpTagControllerTest() {
        prepareExpectedTag();
        prepareExpectedTagList();
        prepareMockTagService();
        prepareMockCriteriaBuilder();
        prepareTagController();
    }

    @Test
    public void shouldFindTags() {
        String[] searchConditions = {"one", "two", "three"};
        String[] sortConditions = {"four", "five", "six"};
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        List<TagDto> actualTags = tagController.find(searchConditions, sortConditions, 1, 50, mockResponse);
        assertEquals(expectedTagList, actualTags);
        verify(mockResponse, times(1)).addHeader("TotalEntityCount", "3");
    }

    @Test
    public void shouldFindTagById() {
        TagDto actualTag = tagController.find(1L);
        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void shouldAddTag() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        tagController.add(expectedTag, mockRequest, mockResponse);
        verify(mockTagService, times(1)).add(expectedTag);
    }

    @Test
    public void shouldEditTag() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);
        tagController.edit(expectedTag, mockRequest, mockResponse);
        verify(mockTagService, times(1)).edit(expectedTag);
    }

    @Test
    public void shouldDeleteTag() {
        tagController.delete(1L);
        verify(mockTagService, times(1)).delete(1L);
    }

    private void prepareExpectedTag() {
        expectedTag = new TagDto(1L, "TestName1");
    }

    private void prepareExpectedTagList() {
        expectedTagList = new ArrayList<>();
        expectedTagList.add(expectedTag);
        expectedTagList.add(new TagDto(2L, "TestName2"));
        expectedTagList.add(new TagDto(3L, "TestName3"));
    }

    private void prepareMockTagService() {
        mockTagService = mock(TagService.class);
        when(mockTagService.get(any(List.class), any(List.class), anyInt(), anyInt())).thenReturn(expectedTagList);
        when(mockTagService.get(anyLong())).thenReturn(expectedTag);
        when(mockTagService.getQuantity(any(List.class))).thenReturn((long) expectedTagList.size());
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.buildSearchCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
        when(mockCriteriaBuilder.buildSortCriteriaList(any(String[].class))).thenReturn(new ArrayList<>());
    }

    private void prepareTagController() {
        tagController = new TagController(mockTagService, mockCriteriaBuilder);
    }
}
package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.specification.QueryOperator;
import com.epam.lab.specification.QueryParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class CriteriaBuilderImplTest {
    private static final String DATE_STR_VALUE = "123123123";
    private static final String TITLE_VALUE = "title";
    private static final String AUTHOR_NAME_VALUE = "name";

    private String[] searchConditions;
    private String[] sortConditions;
    private List<SearchCriteria> expectedSearchCriteriaList;
    private List<SortCriteria> expectedSortCriteriaList;
    private CriteriaBuilder criteriaBuilder;

    @Before
    public void setUpCriteriaBuilderImplTest() {
        prepareSearchConditions();
        prepareSortConditions();
        prepareSearchCriteriaList();
        prepareSortCriteriaList();
        prepareCriteriaBuilder();
    }

    @Test
    public void shouldBuildSearchCriteriaList() {
        List<SearchCriteria> actual = criteriaBuilder.buildSearchCriteriaList(searchConditions);
        assertEquals(expectedSearchCriteriaList, actual);
    }

    @Test
    public void shouldBuildSortCriteriaList() {
        List<SortCriteria> actual = criteriaBuilder.buildSortCriteriaList(sortConditions);
        assertEquals(expectedSortCriteriaList, actual);
    }

    private void prepareSearchConditions() {
        searchConditions = new String[3];
        searchConditions[0] = "title:title";
        searchConditions[1] = "creation_date:123123123";
        searchConditions[2] = "author_name:name";
    }

    private void prepareSortConditions() {
        sortConditions = new String[3];
        sortConditions[0] = "title";
        sortConditions[1] = "-creation_date";
        sortConditions[2] = "modification_date";
    }

    private void prepareSearchCriteriaList() {
        expectedSearchCriteriaList = new ArrayList<>();
        expectedSearchCriteriaList.add(new SearchCriteria(QueryParameter.TITLE, QueryOperator.EQUALS, TITLE_VALUE));
        expectedSearchCriteriaList.add(new SearchCriteria(QueryParameter.CREATION_DATE, QueryOperator.EQUALS, DATE_STR_VALUE));
        expectedSearchCriteriaList.add(new SearchCriteria(QueryParameter.AUTHOR_NAME, QueryOperator.EQUALS, AUTHOR_NAME_VALUE));
    }

    private void prepareSortCriteriaList() {
        expectedSortCriteriaList = new ArrayList<>();
        expectedSortCriteriaList.add(new SortCriteria(QueryParameter.TITLE, true));
        expectedSortCriteriaList.add(new SortCriteria(QueryParameter.CREATION_DATE, false));
        expectedSortCriteriaList.add(new SortCriteria(QueryParameter.MODIFICATION_DATE, true));
    }

    private void prepareCriteriaBuilder() {
        criteriaBuilder = new CriteriaBuilderImpl();
    }
}

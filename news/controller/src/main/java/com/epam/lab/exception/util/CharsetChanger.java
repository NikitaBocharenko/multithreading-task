package com.epam.lab.exception.util;

import java.nio.charset.StandardCharsets;

public class CharsetChanger {

    private CharsetChanger() {
    }

    public static String fromLatin1ToUtf8(String str) {
        return new String(str.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }
}

package com.epam.lab.repository;

import com.epam.lab.model.Role;
import com.epam.lab.model.User;
import com.epam.lab.specification.*;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class UserRepositoryImplTest {
    private static final String RESOURCES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;
    private static final String CREATE_SCHEMA_SQL_FILE_NAME = "create_schema.sql";
    private static final String DROP_SCHEMA_SQL_FILE_NAME = "drop_schema.sql";
    private static final String USER_DATA_FILE_NAME = "user.txt";
    private static final String ROLE_DATA_FILE_NAME = "role.txt";
    private static DataSource dataSource;
    private static Properties hibernateProperties;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private UserRepository userRepository;
    private User expectedUser;
    private List<Role> roleList;

    @BeforeClass
    public static void setUpDataSourceAndHibernateProperties() {
        prepareDataSource();
        prepareHibernateProperties();
    }

    private static void prepareDataSource() {
        HikariConfig hikariConfig = new HikariConfig("/database.properties");
        dataSource = new HikariDataSource(hikariConfig);
    }

    private static void prepareHibernateProperties() {
        hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        hibernateProperties.put("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver");
        hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
    }

    @Before
    public void setUpUserRepositoryTest() throws IOException {
        prepareSchema();
        prepareEntityManagerFactory();
        prepareEntityManager();
        prepareExpectedUser();
        prepareRoleData();
        prepareUserData();
        prepareUserRepository();
    }

    @After
    public void cleanup() throws IOException {
        entityManager.close();
        entityManagerFactory.close();
        dropSchema();
    }

    @Test
    public void shouldFindAll() {
        List<User> actual = userRepository.find(new ArrayList<>(), new ArrayList<>(), 0, 100);
        assertEquals(5, actual.size());
        assertEquals(expectedUser, actual.get(2));
    }

    @Test
    public void shouldFindBySearchSpecification() {
        List<SearchSpecification<User>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.USER_SURNAME, "Bacharenka"));
        List<User> actual = userRepository.find(searchSpecifications, new ArrayList<>(), 0, 100);
        assertEquals(1, actual.size());
        assertEquals(expectedUser, actual.get(0));
    }

    @Test
    public void shouldSortBySortSpecification() {
        List<SortSpecification<User>> sortSpecifications = new ArrayList<>();
        sortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.USER_LOGIN, true));
        List<User> actual = userRepository.find(new ArrayList<>(), sortSpecifications, 0, 100);
        assertEquals(expectedUser, actual.get(2));
    }

    @Test
    public void shouldGetBySpecifiedRange() {
        List<User> actual = userRepository.find(new ArrayList<>(), new ArrayList<>(), 1, 4);
        assertEquals(4, actual.size());
    }

    @Test
    public void shouldGetOne() {
        User actual = userRepository.findOne(3L);
        assertEquals(expectedUser, actual);
    }

    @Test
    public void shouldAdd() {
        User expected = new User();
        expected.setName("Test");
        expected.setSurname("Testov");
        expected.setLogin("test");
        expected.setPassword("1234");
        userRepository.add(expected);
        assertTrue(entityManager.contains(expected));
    }

    @Test
    public void shouldUpdate() {
        expectedUser.setName("Updated");
        userRepository.set(expectedUser);
        User actual = entityManager.find(User.class, 3L);
        assertEquals(expectedUser, actual);
    }

    @Test
    public void shouldDelete() {
        userRepository.remove(1L);
        User actual = entityManager.find(User.class, 1L);
        assertNull(actual);
    }

    @Test
    public void shouldCountAll() {
        long actual = userRepository.count(new ArrayList<>());
        assertEquals(5, actual);
    }

    @Test
    public void shouldCountSpecified() {
        List<SearchSpecification<User>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.USER_NAME, "Mikita"));
        long actual = userRepository.count(searchSpecifications);
        assertEquals(1, actual);
    }

    private void prepareSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + CREATE_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void dropSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + DROP_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void prepareEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.epam.lab.model");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(hibernateProperties);
        em.setPersistenceUnitName("news_management");
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.afterPropertiesSet();
        entityManagerFactory = em.getObject();
    }

    private void prepareEntityManager() {
        entityManager = entityManagerFactory.createEntityManager();
    }

    private void prepareExpectedUser() {
        Set<Role> roles = new HashSet<>();
        roles.add(new Role(3L, "ADMIN"));
        expectedUser = new User(3L, "Mikita", "Bacharenka", "mikita", "111", roles);
    }

    private void prepareRoleData() throws IOException {
        roleList = new ArrayList<>();
        Path path = Paths.get(RESOURCES_PATH + ROLE_DATA_FILE_NAME);
        List<String> lines = Files.readAllLines(path);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        for (String data : lines) {
            Role role = new Role();
            role.setName(data);
            roleList.add(role);
            entityManager.persist(role);
        }
        entityTransaction.commit();
    }

    private void prepareUserData() throws IOException {
        Path path = Paths.get(RESOURCES_PATH + USER_DATA_FILE_NAME);
        List<String> lines = Files.readAllLines(path);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        int i = 0;
        for (String line : lines) {
            String[] data = line.split(",");
            User user = new User();
            user.setName(data[0]);
            user.setSurname(data[1]);
            user.setLogin(data[2]);
            user.setPassword(data[3]);
            Set<Role> roles = new HashSet<>();
            roles.add(roleList.get(i++ % 3));
            user.setRoleSet(roles);
            entityManager.persist(user);
        }
        entityTransaction.commit();
    }

    private void prepareUserRepository() {
        userRepository = new UserRepositoryImpl(entityManager);
    }
}

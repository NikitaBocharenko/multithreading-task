package com.epam.lab.specification;

import com.epam.lab.model.News;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class SortSpecificationImplTest {
    private Root<News> mockRoot;
    private CriteriaBuilder mockCriteriaBuilder;
    private QueryParameter specifiedParameter;
    private Path<Object> mockPathObject;
    private Order mockAscOrder;
    private Order mockDescOrder;

    @Before
    public void setUpJpaSortSpecificationImplTest() {
        prepareSpecifiedParameter();
        prepareMockPathObject();
        prepareMockRoot();
        prepareMockAscOrder();
        prepareMockDescOrder();
        prepareMockCriteriaBuilder();
    }

    @Test
    public void shouldGetAscOrder() {
        SortSpecificationImpl<News> specification = new SortSpecificationImpl<>(specifiedParameter, true);
        Order actual = specification.toOrder(mockRoot, mockCriteriaBuilder);
        assertEquals(mockAscOrder, actual);
    }

    @Test
    public void shouldGetDescOrder() {
        SortSpecificationImpl<News> specification = new SortSpecificationImpl<>(specifiedParameter, false);
        Order actual = specification.toOrder(mockRoot, mockCriteriaBuilder);
        assertEquals(mockDescOrder, actual);
    }


    private void prepareSpecifiedParameter() {
        specifiedParameter = QueryParameter.TITLE;
    }

    private void prepareMockPathObject() {
        mockPathObject = mock(Path.class);
    }

    private void prepareMockRoot() {
        mockRoot = mock(Root.class);
        when(mockRoot.get(QueryParameter.TITLE.getParameterName())).thenReturn(mockPathObject);
    }

    private void prepareMockAscOrder() {
        mockAscOrder = mock(Order.class);
    }

    private void prepareMockDescOrder() {
        mockDescOrder = mock(Order.class);
    }

    private void prepareMockCriteriaBuilder() {
        mockCriteriaBuilder = mock(CriteriaBuilder.class);
        when(mockCriteriaBuilder.asc(mockPathObject)).thenReturn(mockAscOrder);
        when(mockCriteriaBuilder.desc(mockPathObject)).thenReturn(mockDescOrder);
    }
}

package com.epam.lab.repository;

import com.epam.lab.model.Tag;
import com.epam.lab.specification.*;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TagRepositoryImplTest {
    private static final String RESOURCES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;
    private static final String CREATE_SCHEMA_SQL_FILE_NAME = "create_schema.sql";
    private static final String DROP_SCHEMA_SQL_FILE_NAME = "drop_schema.sql";
    private static final String TAG_DATA_FILE_NAME = "tag.txt";
    private static DataSource dataSource;
    private static Properties hibernateProperties;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private TagRepository tagRepository;
    private Tag expectedTag;

    @BeforeClass
    public static void setUpDataSourceAndHibernateProperties() {
        prepareDataSource();
        prepareHibernateProperties();
    }

    private static void prepareDataSource() {
        HikariConfig hikariConfig = new HikariConfig("/database.properties");
        dataSource = new HikariDataSource(hikariConfig);
    }

    private static void prepareHibernateProperties() {
        hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        hibernateProperties.put("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver");
        hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
    }

    @Before
    public void setUpTagRepositoryTest() throws IOException {
        prepareSchema();
        prepareEntityManagerFactory();
        prepareEntityManager();
        prepareExpectedTag();
        prepareData();
        prepareTagRepository();
    }

    @After
    public void cleanup() throws IOException {
        entityManager.close();
        entityManagerFactory.close();
        dropSchema();
    }

    @Test
    public void shouldFindBySearchSpecification() {
        List<SearchSpecification<Tag>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.TAG_NAME, "politics"));
        List<Tag> actual = tagRepository.find(searchSpecifications, new ArrayList<>(), 0, 100);
        assertEquals(1, actual.size());
        assertEquals(expectedTag, actual.get(0));
    }

    @Test
    public void shouldSortBySortSpecification() {
        List<SortSpecification<Tag>> sortSpecifications = new ArrayList<>();
        sortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.TAG_NAME, false));
        List<Tag> actual = tagRepository.find(new ArrayList<>(), sortSpecifications, 0, 100);
        assertEquals(expectedTag, actual.get(2));
    }

    @Test
    public void shouldGetBySpecifiedRange() {
        List<Tag> actual = tagRepository.find(new ArrayList<>(), new ArrayList<>(), 1, 4);
        assertEquals(4, actual.size());
    }

    @Test
    public void shouldGetOne() {
        Tag actual = tagRepository.findOne(1L);
        assertEquals(expectedTag, actual);
    }

    @Test
    public void shouldAdd() {
        Tag expected = new Tag();
        expected.setName("Test");
        tagRepository.add(expected);
        assertTrue(entityManager.contains(expected));
    }

    @Test
    public void shouldUpdate() {
        expectedTag.setName("Updated");
        tagRepository.set(expectedTag);
        Tag actual = entityManager.find(Tag.class, 1L);
        assertEquals(expectedTag, actual);
    }

    @Test
    public void shouldDelete() {
        tagRepository.remove(1L);
        Tag actual = entityManager.find(Tag.class, 1L);
        assertNull(actual);
    }

    @Test
    public void shouldCountAll() {
        long actual = tagRepository.count(new ArrayList<>());
        assertEquals(5, actual);
    }

    @Test
    public void shouldCountSpecified() {
        List<SearchSpecification<Tag>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.TAG_NAME, "politics"));
        long actual = tagRepository.count(searchSpecifications);
        assertEquals(1, actual);
    }

    private void prepareSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + CREATE_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void dropSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + DROP_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void prepareEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.epam.lab.model");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(hibernateProperties);
        em.setPersistenceUnitName("news_management");
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.afterPropertiesSet();
        entityManagerFactory = em.getObject();
    }

    private void prepareEntityManager() {
        entityManager = entityManagerFactory.createEntityManager();
    }

    private void prepareExpectedTag() {
        expectedTag = new Tag();
        expectedTag.setId(1L);
        expectedTag.setName("politics");
    }

    private void prepareData() throws IOException {
        Path path = Paths.get(RESOURCES_PATH + TAG_DATA_FILE_NAME);
        List<String> lines = Files.readAllLines(path);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        for (String line : lines) {
            Tag tag = new Tag();
            tag.setName(line);
            entityManager.persist(tag);
        }
        entityTransaction.commit();
    }

    private void prepareTagRepository() {
        tagRepository = new TagRepositoryImpl(entityManager);
    }
}

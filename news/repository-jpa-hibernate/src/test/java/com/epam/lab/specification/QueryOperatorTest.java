package com.epam.lab.specification;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class QueryOperatorTest {
    private static final String EQUALS_STR = ":";
    private static final String GREATER_STR = ">";
    private static final String GREATER_OR_EQUALS_STR = ">:";
    private static final String LOWER_STR = "<";
    private static final String LOWER_OR_EQUALS_STR = "<:";

    @Test
    public void shouldReturnEqualsOperator() {
        QueryOperator operator = QueryOperator.fromString(EQUALS_STR).orElseThrow(IllegalArgumentException::new);
        assertEquals(QueryOperator.EQUALS, operator);
    }

    @Test
    public void shouldReturnGreaterThanOperator() {
        QueryOperator operator = QueryOperator.fromString(GREATER_STR).orElseThrow(IllegalArgumentException::new);
        assertEquals(QueryOperator.GREATER, operator);
    }

    @Test
    public void shouldReturnLowerThanOperator() {
        QueryOperator operator = QueryOperator.fromString(LOWER_STR).orElseThrow(IllegalArgumentException::new);
        assertEquals(QueryOperator.LOWER, operator);
    }

    @Test
    public void shouldReturnGreaterOrEqualsOperator() {
        QueryOperator operator = QueryOperator.fromString(GREATER_OR_EQUALS_STR).orElseThrow(IllegalArgumentException::new);
        assertEquals(QueryOperator.GREATER_OR_EQUALS, operator);
    }

    @Test
    public void shouldReturnLowerOrEqualsOperator() {
        QueryOperator operator = QueryOperator.fromString(LOWER_OR_EQUALS_STR).orElseThrow(IllegalArgumentException::new);
        assertEquals(QueryOperator.LOWER_OR_EQUALS, operator);
    }
}

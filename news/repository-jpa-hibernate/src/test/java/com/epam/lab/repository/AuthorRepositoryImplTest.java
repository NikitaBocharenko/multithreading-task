package com.epam.lab.repository;

import com.epam.lab.model.Author;
import com.epam.lab.specification.*;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AuthorRepositoryImplTest {
    private static final String RESOURCES_PATH = "src" + File.separator + "test" + File.separator + "resources" + File.separator;
    private static final String CREATE_SCHEMA_SQL_FILE_NAME = "create_schema.sql";
    private static final String DROP_SCHEMA_SQL_FILE_NAME = "drop_schema.sql";
    private static final String AUTHOR_DATA_FILE_NAME = "author.txt";
    private static DataSource dataSource;
    private static Properties hibernateProperties;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;
    private AuthorRepository authorRepository;
    private Author expectedAuthor;

    @BeforeClass
    public static void setUpDataSourceAndHibernateProperties() {
        prepareDataSource();
        prepareHibernateProperties();
    }

    private static void prepareDataSource() {
        HikariConfig hikariConfig = new HikariConfig("/database.properties");
        dataSource = new HikariDataSource(hikariConfig);
    }

    private static void prepareHibernateProperties() {
        hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        hibernateProperties.put("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver");
        hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
    }

    @Before
    public void setUpAuthorRepositoryTest() throws IOException {
        prepareSchema();
        prepareEntityManagerFactory();
        prepareEntityManager();
        prepareExpectedAuthor();
        prepareData();
        prepareAuthorRepository();
    }

    @After
    public void cleanup() throws IOException {
        entityManager.close();
        entityManagerFactory.close();
        dropSchema();
    }

    @Test
    public void shouldFindBySearchSpecification() {
        List<SearchSpecification<Author>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.AUTHOR_SURNAME, "Titov"));
        List<Author> actual = authorRepository.find(searchSpecifications, new ArrayList<>(), 0, 100);
        assertEquals(2, actual.size());
    }

    @Test
    public void shouldSortBySortSpecification() {
        List<SortSpecification<Author>> sortSpecifications = new ArrayList<>();
        sortSpecifications.add(new SortSpecificationImpl<>(QueryParameter.AUTHOR_NAME, true));
        List<Author> actual = authorRepository.find(new ArrayList<>(), sortSpecifications, 0, 100);
        assertEquals(expectedAuthor, actual.get(2));
    }

    @Test
    public void shouldGetBySpecifiedRange() {
        List<Author> actual = authorRepository.find(new ArrayList<>(), new ArrayList<>(), 1, 4);
        assertEquals(4, actual.size());
    }

    @Test
    public void shouldGetOne() {
        Author actual = authorRepository.findOne(1L);
        assertEquals(expectedAuthor, actual);
    }

    @Test
    public void shouldAdd() {
        Author expected = new Author();
        expected.setName("Test");
        expected.setSurname("Testov");
        authorRepository.add(expected);
        assertTrue(entityManager.contains(expected));
    }

    @Test
    public void shouldUpdate() {
        expectedAuthor.setName("Updated");
        authorRepository.set(expectedAuthor);
        Author actual = entityManager.find(Author.class, 1L);
        assertEquals(expectedAuthor, actual);
    }

    @Test
    public void shouldDelete() {
        authorRepository.remove(1L);
        Author actual = entityManager.find(Author.class, 1L);
        assertNull(actual);
    }

    @Test
    public void shouldCountAll() {
        long actual = authorRepository.count(new ArrayList<>());
        assertEquals(5, actual);
    }

    @Test
    public void shouldCountSpecified() {
        List<SearchSpecification<Author>> searchSpecifications = new ArrayList<>();
        searchSpecifications.add(new DefaultStringSearchSpecification<>(QueryParameter.AUTHOR_SURNAME, "Titov"));
        long actual = authorRepository.count(searchSpecifications);
        assertEquals(2, actual);
    }

    private void prepareSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + CREATE_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void dropSchema() throws IOException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Path path = Paths.get(RESOURCES_PATH + DROP_SCHEMA_SQL_FILE_NAME);
        List<String> queries = Files.readAllLines(path);
        jdbcTemplate.execute(queries.get(0));
    }

    private void prepareEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.epam.lab.model");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(hibernateProperties);
        em.setPersistenceUnitName("news_management");
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.afterPropertiesSet();
        entityManagerFactory = em.getObject();
    }

    private void prepareEntityManager() {
        entityManager = entityManagerFactory.createEntityManager();
    }

    private void prepareExpectedAuthor() {
        expectedAuthor = new Author();
        expectedAuthor.setId(1L);
        expectedAuthor.setName("Mikita");
        expectedAuthor.setSurname("Bacharenka");
    }

    private void prepareData() throws IOException {
        Path path = Paths.get(RESOURCES_PATH + AUTHOR_DATA_FILE_NAME);
        List<String> lines = Files.readAllLines(path);
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        for (String line : lines) {
            String[] data = line.split(",");
            Author author = new Author();
            author.setName(data[0]);
            author.setSurname(data[1]);
            entityManager.persist(author);
        }
        entityTransaction.commit();
    }

    private void prepareAuthorRepository() {
        authorRepository = new AuthorRepositoryImpl(entityManager);
    }

}

package com.epam.lab.exception;

public class AuthorNotFoundException extends EntityNotFoundException {
    public AuthorNotFoundException(long id) {
        super("author", id);
    }
}

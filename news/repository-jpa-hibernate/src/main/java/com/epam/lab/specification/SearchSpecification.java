package com.epam.lab.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


public interface SearchSpecification<T> {
    Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder);
}

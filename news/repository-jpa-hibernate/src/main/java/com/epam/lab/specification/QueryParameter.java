package com.epam.lab.specification;

import java.util.Optional;
import java.util.stream.Stream;

public enum QueryParameter {
    TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE,
    AUTHOR_NAME, AUTHOR_SURNAME, TAG_NAME,
    USER_NAME, USER_SURNAME, USER_LOGIN;

    public static Optional<QueryParameter> fromString(String fieldName) {
        return Stream.of(QueryParameter.values())
                .filter(queryParameter -> queryParameter.toString().equalsIgnoreCase(fieldName))
                .findFirst();
    }

    public String getParameterName() {
        switch (this) {
            case TITLE:
                return "title";
            case SHORT_TEXT:
                return "shortText";
            case FULL_TEXT:
                return "fullText";
            case CREATION_DATE:
                return "creationDate";
            case MODIFICATION_DATE:
                return "modificationDate";
            case AUTHOR_NAME:
            case TAG_NAME:
            case USER_NAME:
                return "name";
            case AUTHOR_SURNAME:
            case USER_SURNAME:
                return "surname";
            case USER_LOGIN:
                return "login";
            default:
                throw new IllegalArgumentException("Wrong QueryParameter enum");
        }
    }

    public String getEntityName() {
        switch (this) {
            case AUTHOR_NAME:
            case AUTHOR_SURNAME:
                return "author";
            case TAG_NAME:
                return "tagSet";
            default:
                throw new IllegalArgumentException("Wrong QueryParameter enum");
        }
    }
}

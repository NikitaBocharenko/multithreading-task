package com.epam.lab.repository;

import com.epam.lab.exception.AuthorNotFoundException;
import com.epam.lab.model.Author;
import com.epam.lab.specification.ConditionBuilder;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class AuthorRepositoryImpl implements AuthorRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public AuthorRepositoryImpl() {
    }

    public AuthorRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(Author entity) {
        entityManager.persist(entity);
    }

    @Override
    public void set(Author entity) {
        findOne(entity.getId());
        entityManager.merge(entity);
    }

    @Override
    public void remove(Long id) {
        Author entity = findOne(id);
        entityManager.remove(entity);
    }

    @Override
    public Author findOne(Long id) {
        Author author = entityManager.find(Author.class, id);
        if (author == null) {
            throw new AuthorNotFoundException(id);
        }
        return author;
    }

    @Override
    public List<Author> find(List<SearchSpecification<Author>> searchSpecifications, List<SortSpecification<Author>> sortSpecifications, int offset, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
        Root<Author> authorRoot = criteriaQuery.from(Author.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, authorRoot, criteriaBuilder);
        Order[] orders = ConditionBuilder.buildOrders(sortSpecifications, authorRoot, criteriaBuilder);
        criteriaQuery.select(authorRoot).where(predicates).orderBy(orders);
        TypedQuery<Author> query = entityManager.createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public boolean isAuthorAlreadyPersisted(Author author) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
        Root<Author> authorRoot = criteriaQuery.from(Author.class);
        criteriaQuery.select(authorRoot).where(criteriaBuilder.equal(authorRoot.get("name"), author.getName()),
                criteriaBuilder.equal(authorRoot.get("surname"), author.getSurname()));
        TypedQuery<Author> query = entityManager.createQuery(criteriaQuery);
        return !query.getResultList().isEmpty();
    }

    @Override
    public Long count(List<SearchSpecification<Author>> searchSpecifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Author> authorRoot = criteriaQuery.from(Author.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, authorRoot, criteriaBuilder);
        criteriaQuery.select(criteriaBuilder.count(authorRoot)).where(predicates);
        TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
        return query.getSingleResult();
    }
}

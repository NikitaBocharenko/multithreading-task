package com.epam.lab.specification;

import com.epam.lab.model.News;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Objects;

public class NewsByAuthorStringSearchSpecification implements SearchSpecification<News> {
    private QueryParameter queryParameter;
    private String value;

    public NewsByAuthorStringSearchSpecification(QueryParameter queryParameter, String value) {
        this.queryParameter = queryParameter;
        this.value = value;
    }

    @Override
    public Predicate toPredicate(Root<News> root, CriteriaBuilder criteriaBuilder) {
        String entityName = queryParameter.getEntityName();
        String fieldName = queryParameter.getParameterName();
        return criteriaBuilder.equal(root.get(entityName).get(fieldName), value);
    }

    @Override
    public String toString() {
        return "NewsByAuthorStringJpaSearchSpecification{" +
                "queryParameter=" + queryParameter +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsByAuthorStringSearchSpecification that = (NewsByAuthorStringSearchSpecification) o;
        return queryParameter == that.queryParameter &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryParameter, value);
    }
}

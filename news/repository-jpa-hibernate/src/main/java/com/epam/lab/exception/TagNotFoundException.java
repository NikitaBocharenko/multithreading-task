package com.epam.lab.exception;

public class TagNotFoundException extends EntityNotFoundException {
    public TagNotFoundException(long id) {
        super("tag", id);
    }
}

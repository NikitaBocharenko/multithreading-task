package com.epam.lab.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Objects;

public class DefaultStringSearchSpecification<T> implements SearchSpecification<T> {
    private QueryParameter queryParameter;
    private String value;

    public DefaultStringSearchSpecification(QueryParameter queryParameter, String value) {
        this.queryParameter = queryParameter;
        this.value = value;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.equal(root.get(queryParameter.getParameterName()), value);
    }

    @Override
    public String toString() {
        return "DefaultStringJpaSearchSpecification{" +
                "queryParameter=" + queryParameter +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultStringSearchSpecification<?> that = (DefaultStringSearchSpecification<?>) o;
        return queryParameter == that.queryParameter &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryParameter, value);
    }
}

package com.epam.lab.repository;

import com.epam.lab.exception.UserNotFoundException;
import com.epam.lab.model.User;
import com.epam.lab.specification.ConditionBuilder;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public UserRepositoryImpl() {
    }

    public UserRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(User entity) {
        entityManager.persist(entity);
    }

    @Override
    public void set(User entity) {
        findOne(entity.getId());
        entityManager.merge(entity);
    }

    @Override
    public void remove(Long id) {
        User entity = findOne(id);
        entityManager.remove(entity);
    }

    @Override
    public User findOne(Long id) {
        User user = entityManager.find(User.class, id);
        if (user == null) {
            throw new UserNotFoundException(id);
        }
        return user;
    }

    @Override
    public List<User> find(List<SearchSpecification<User>> searchSpecifications, List<SortSpecification<User>> sortSpecifications, int offset, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, userRoot, criteriaBuilder);
        Order[] orders = ConditionBuilder.buildOrders(sortSpecifications, userRoot, criteriaBuilder);
        criteriaQuery.select(userRoot).where(predicates).orderBy(orders);
        TypedQuery<User> query = entityManager.createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public Long count(List<SearchSpecification<User>> searchSpecifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, userRoot, criteriaBuilder);
        criteriaQuery.select(criteriaBuilder.count(userRoot)).where(predicates);
        TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
        return query.getSingleResult();
    }
}

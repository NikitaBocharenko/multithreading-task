package com.epam.lab.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class ConditionBuilder {
    private ConditionBuilder() {
    }

    public static <T> Predicate[] buildPredicates(List<SearchSpecification<T>> searchSpecifications, Root<T> root, CriteriaBuilder criteriaBuilder) {
        Predicate[] predicates = new Predicate[searchSpecifications.size()];
        for (int i = 0; i < searchSpecifications.size(); i++) {
            predicates[i] = searchSpecifications.get(i).toPredicate(root, criteriaBuilder);
        }
        return predicates;
    }

    public static <T> Order[] buildOrders(List<SortSpecification<T>> sortSpecifications, Root<T> root, CriteriaBuilder criteriaBuilder) {
        Order[] orders = new Order[sortSpecifications.size()];
        for (int i = 0; i < sortSpecifications.size(); i++) {
            orders[i] = sortSpecifications.get(i).toOrder(root, criteriaBuilder);
        }
        return orders;
    }
}

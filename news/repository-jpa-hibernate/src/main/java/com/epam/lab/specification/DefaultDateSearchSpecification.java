package com.epam.lab.specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.Objects;

public class DefaultDateSearchSpecification<T> implements SearchSpecification<T> {
    private QueryParameter queryParameter;
    private LocalDate value;

    public DefaultDateSearchSpecification(QueryParameter queryParameter, LocalDate value) {
        this.queryParameter = queryParameter;
        this.value = value;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.equal(root.get(queryParameter.getParameterName()), value);
    }

    @Override
    public String toString() {
        return "DefaultDateJpaSearchSpecification{" +
                "queryParameter=" + queryParameter +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultDateSearchSpecification<?> that = (DefaultDateSearchSpecification<?>) o;
        return queryParameter == that.queryParameter &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryParameter, value);
    }
}

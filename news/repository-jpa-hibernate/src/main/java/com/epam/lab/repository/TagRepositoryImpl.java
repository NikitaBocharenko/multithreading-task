package com.epam.lab.repository;

import com.epam.lab.exception.TagNotFoundException;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.specification.ConditionBuilder;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Set;

@Repository
public class TagRepositoryImpl implements TagRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public TagRepositoryImpl() {
    }

    public TagRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(Tag entity) {
        entityManager.persist(entity);
    }

    @Override
    public void set(Tag entity) {
        findOne(entity.getId());
        entityManager.merge(entity);
    }

    @Override
    public void remove(Long id) {
        Tag entity = findOne(id);
        Set<News> newsWithThisTag = entity.getNewsSet();
        for (News news : newsWithThisTag) {
            news.getTagSet().remove(entity);
        }
        entityManager.remove(entity);
    }

    @Override
    public Tag findOne(Long id) {
        Tag tag = entityManager.find(Tag.class, id);
        if (tag == null) {
            throw new TagNotFoundException(id);
        }
        return tag;
    }

    @Override
    public List<Tag> find(List<SearchSpecification<Tag>> searchSpecifications, List<SortSpecification<Tag>> sortSpecifications, int offset, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> tagRoot = criteriaQuery.from(Tag.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, tagRoot, criteriaBuilder);
        Order[] orders = ConditionBuilder.buildOrders(sortSpecifications, tagRoot, criteriaBuilder);
        criteriaQuery.select(tagRoot).where(predicates).orderBy(orders);
        TypedQuery<Tag> query = entityManager.createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public boolean isTagAlreadyPersisted(Tag tag) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> tagRoot = criteriaQuery.from(Tag.class);
        criteriaQuery.select(tagRoot).where(criteriaBuilder.equal(tagRoot.get("name"), tag.getName()));
        TypedQuery<Tag> query = entityManager.createQuery(criteriaQuery);
        return !query.getResultList().isEmpty();
    }

    @Override
    public Long count(List<SearchSpecification<Tag>> searchSpecifications) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<Tag> tagRoot = criteriaQuery.from(Tag.class);
        Predicate[] predicates = ConditionBuilder.buildPredicates(searchSpecifications, tagRoot, criteriaBuilder);
        criteriaQuery.select(criteriaBuilder.count(tagRoot)).where(predicates);
        TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
        return query.getSingleResult();
    }
}

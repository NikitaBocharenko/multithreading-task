package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.model.News;
import com.epam.lab.specification.DefaultDateSearchSpecification;
import com.epam.lab.specification.QueryOperator;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class DefaultDateSearchSpecificationBuilderTest {
    private static final String DATE_STR = "2019-01-02";
    private DefaultDateSearchSpecification<News> expected;
    private SearchCriteria searchCriteria;

    @Before
    public void setUpDefaultDateSearchSpecificationBuilderTest() {
        prepareSearchCriteria();
        prepareExpectedSpecification();
    }

    @Test
    public void shouldBuildSpecification() {
        DefaultDateSearchSpecificationBuilder<News> builder = new DefaultDateSearchSpecificationBuilder<>();
        SearchSpecification<News> actual = builder.build(searchCriteria);
        assertEquals(expected, actual);
    }

    private void prepareSearchCriteria() {
        searchCriteria = new SearchCriteria(QueryParameter.CREATION_DATE, QueryOperator.EQUALS, DATE_STR);
    }

    private void prepareExpectedSpecification() {
        LocalDate date = LocalDate.parse(DATE_STR);
        expected = new DefaultDateSearchSpecification<>(QueryParameter.CREATION_DATE, date);
    }
}

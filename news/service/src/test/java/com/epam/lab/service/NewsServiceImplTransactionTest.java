package com.epam.lab.service;

import com.epam.lab.configuration.SpringServiceConfiguration;
import com.epam.lab.dto.NewsDto;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TestTransaction;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@SpringJUnitConfig({JpaTestConfiguration.class, SpringServiceConfiguration.class})
@RunWith(SpringRunner.class)
@Transactional
public class NewsServiceImplTransactionTest extends AbstractTransactionalJUnit4SpringContextTests {
    private static final Logger LOG = Logger.getLogger(NewsServiceImplTransactionTest.class);

    private NewsDto newsDto;

    @Autowired
    private NewsService newsService;

    @PersistenceContext
    private EntityManager entityManager;


    @BeforeTransaction
    public void setUpTestData() {
        Author author = new Author(null, "TestName", "TestSurname");
        Set<Tag> tagSet = new HashSet<>();
        tagSet.add(new Tag(null, "TestTag1"));
        tagSet.add(new Tag(null, "TestTag2"));
        News news = new News(null, "Test title", "Test short text", "Test full text",
                LocalDate.of(2020, 1, 11), LocalDate.of(2020, 2, 12),
                author, tagSet);
        newsDto = new NewsDto(news, author, tagSet);
    }

    @Test
    @Commit
    public void shouldCommitTransaction() {
        newsService.add(newsDto);
        entityManager.flush();
        TestTransaction.end();

        int newsCount = countRowsInTable("nm.news");
        assertEquals(1, newsCount);
        int authorCount = countRowsInTable("nm.author");
        assertEquals(1, authorCount);
        int tagsCount = countRowsInTable("nm.tag");
        assertEquals(2, tagsCount);
    }

    @Test
    public void shouldRollbackTransaction() {
        newsDto.setId(10L);
        try {
            newsService.add(newsDto);
        } catch (InvalidDataAccessApiUsageException e) {
            LOG.info(e.getClass() + " " + e.getMessage());
        }
        entityManager.flush();
        TestTransaction.end();

        int newsCount = countRowsInTable("nm.news");
        assertEquals(0, newsCount);
        int authorCount = countRowsInTable("nm.author");
        assertEquals(0, authorCount);
        int tagsCount = countRowsInTable("nm.tag");
        assertEquals(0, tagsCount);
    }

}

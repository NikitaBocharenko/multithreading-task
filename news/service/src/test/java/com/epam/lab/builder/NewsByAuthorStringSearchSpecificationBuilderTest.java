package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.model.News;
import com.epam.lab.specification.NewsByAuthorStringSearchSpecification;
import com.epam.lab.specification.QueryOperator;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class NewsByAuthorStringSearchSpecificationBuilderTest {
    private static final String VALUE = "value";
    private NewsByAuthorStringSearchSpecification expected;
    private SearchCriteria searchCriteria;

    @Before
    public void setUpDefaultDateSearchSpecificationBuilderTest() {
        prepareSearchCriteria();
        prepareExpectedSpecification();
    }

    @Test
    public void shouldBuildSpecification() {
        NewsByAuthorStringSearchSpecificationBuilder builder = new NewsByAuthorStringSearchSpecificationBuilder();
        SearchSpecification<News> actual = builder.build(searchCriteria);
        assertEquals(expected, actual);
    }

    private void prepareSearchCriteria() {
        searchCriteria = new SearchCriteria(QueryParameter.AUTHOR_NAME, QueryOperator.EQUALS, VALUE);
    }

    private void prepareExpectedSpecification() {
        expected = new NewsByAuthorStringSearchSpecification(QueryParameter.AUTHOR_NAME, VALUE);
    }
}

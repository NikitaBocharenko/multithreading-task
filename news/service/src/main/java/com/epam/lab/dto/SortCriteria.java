package com.epam.lab.dto;

import com.epam.lab.specification.QueryParameter;

import java.util.Objects;

public class SortCriteria {
    private QueryParameter queryParameter;
    private boolean ascending;

    public SortCriteria(QueryParameter queryParameter, boolean ascending) {
        this.queryParameter = queryParameter;
        this.ascending = ascending;
    }

    public QueryParameter getQueryParameter() {
        return queryParameter;
    }

    public boolean isAscending() {
        return ascending;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SortCriteria that = (SortCriteria) o;
        return ascending == that.ascending &&
                queryParameter == that.queryParameter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryParameter, ascending);
    }
}

package com.epam.lab.builder;

import com.epam.lab.specification.QueryParameter;

import java.util.EnumMap;
import java.util.Map;

public class SearchSpecificationBuilderProducerImpl<T> implements SearchSpecificationBuilderProducer<T> {
    private Map<QueryParameter, SearchSpecificationBuilder<T>> specificationMap = new EnumMap<>(QueryParameter.class);

    @Override
    public SearchSpecificationBuilder<T> get(QueryParameter queryParameter) {
        return specificationMap.get(queryParameter);
    }

    public void add(QueryParameter key, SearchSpecificationBuilder<T> value) {
        specificationMap.put(key, value);
    }
}

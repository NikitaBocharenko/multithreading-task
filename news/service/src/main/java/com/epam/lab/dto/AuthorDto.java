package com.epam.lab.dto;

import com.epam.lab.model.Author;
import com.epam.lab.validation.AddingValidation;
import com.epam.lab.validation.UpdatingValidation;

import javax.validation.constraints.*;
import java.util.Objects;

public class AuthorDto {
    @Null(message = "Id must be absent", groups = AddingValidation.class)
    @NotNull(message = "Id must be not null", groups = UpdatingValidation.class)
    @Positive(message = "Id must be positive", groups = UpdatingValidation.class)
    private Long id;

    @NotBlank(message = "Author name must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "Author name must be no larger than 30 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String name;

    @NotBlank(message = "Author surname must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "Author surname must be no larger than 30 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String surname;

    public AuthorDto() {
    }

    public AuthorDto(Long id, String name, String surname) {
        this.setId(id);
        this.setName(name);
        this.setSurname(surname);
    }

    public AuthorDto(Author author) {
        this.setId(author.getId());
        this.setName(author.getName());
        this.setSurname(author.getSurname());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorDto authorDTO = (AuthorDto) o;
        return Objects.equals(id, authorDTO.id) &&
                Objects.equals(name, authorDTO.name) &&
                Objects.equals(surname, authorDTO.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname);
    }

    @Override
    public String toString() {
        return "AuthorDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}

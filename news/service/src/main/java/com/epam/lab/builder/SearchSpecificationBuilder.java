package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.specification.SearchSpecification;

public interface SearchSpecificationBuilder<T> {
    SearchSpecification<T> build(SearchCriteria searchCriteria);
}

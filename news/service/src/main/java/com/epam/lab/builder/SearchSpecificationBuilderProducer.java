package com.epam.lab.builder;

import com.epam.lab.specification.QueryParameter;

public interface SearchSpecificationBuilderProducer<T> {
    SearchSpecificationBuilder<T> get(QueryParameter queryParameter);
}

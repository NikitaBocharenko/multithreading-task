package com.epam.lab.configuration;

import com.epam.lab.builder.*;
import com.epam.lab.dto.NewsDto;
import com.epam.lab.dto.UserDto;
import com.epam.lab.model.Author;
import com.epam.lab.model.News;
import com.epam.lab.model.Tag;
import com.epam.lab.model.User;
import com.epam.lab.specification.QueryParameter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@ComponentScan("com.epam.lab.service")
@EnableTransactionManagement
public class SpringServiceConfiguration {
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean
    public SearchSpecificationBuilderProducer<Author> authorSearchSpecificationBuilderProducer() {
        SearchSpecificationBuilderProducerImpl<Author> builderProducer = new SearchSpecificationBuilderProducerImpl<>();
        builderProducer.add(QueryParameter.AUTHOR_NAME, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.AUTHOR_SURNAME, new DefaultStringSearchSpecificationBuilder<>());
        return builderProducer;
    }

    @Bean
    public SearchSpecificationBuilderProducer<Tag> tagSearchSpecificationBuilderProducer() {
        SearchSpecificationBuilderProducerImpl<Tag> builderProducer = new SearchSpecificationBuilderProducerImpl<>();
        builderProducer.add(QueryParameter.TAG_NAME, new DefaultStringSearchSpecificationBuilder<>());
        return builderProducer;
    }

    @Bean
    public SearchSpecificationBuilderProducer<News> newsSearchSpecificationBuilderProducer() {
        SearchSpecificationBuilderProducerImpl<News> builderProducer = new SearchSpecificationBuilderProducerImpl<>();
        builderProducer.add(QueryParameter.TITLE, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.SHORT_TEXT, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.FULL_TEXT, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.CREATION_DATE, new DefaultDateSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.MODIFICATION_DATE, new DefaultDateSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.AUTHOR_NAME, new NewsByAuthorStringSearchSpecificationBuilder());
        builderProducer.add(QueryParameter.AUTHOR_SURNAME, new NewsByAuthorStringSearchSpecificationBuilder());
        builderProducer.add(QueryParameter.TAG_NAME, new NewsByTagStringSearchSpecificationBuilder());
        return builderProducer;
    }

    @Bean
    public SearchSpecificationBuilderProducer<User> userSearchSpecificationBuilderProducer() {
        SearchSpecificationBuilderProducerImpl<User> builderProducer = new SearchSpecificationBuilderProducerImpl<>();
        builderProducer.add(QueryParameter.USER_NAME, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.USER_SURNAME, new DefaultStringSearchSpecificationBuilder<>());
        builderProducer.add(QueryParameter.USER_LOGIN, new DefaultStringSearchSpecificationBuilder<>());
        return builderProducer;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.typeMap(News.class, NewsDto.class)
                .addMapping(News::getAuthor, NewsDto::setAuthorDto)
                .addMapping(News::getTagSet, NewsDto::setTagDtoSet);
        modelMapper.typeMap(User.class, UserDto.class)
                .addMapping(User::getLogin, UserDto::setUsername)
                .addMapping(User::getRoleSet, UserDto::setAuthorities);
        modelMapper.typeMap(UserDto.class, User.class)
                .addMapping(UserDto::getUsername, User::setLogin)
                .addMapping(UserDto::getAuthorities, User::setRoleSet);
        return modelMapper;
    }
}

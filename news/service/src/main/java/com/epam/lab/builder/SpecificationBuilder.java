package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.dto.SortCriteria;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;
import com.epam.lab.specification.SortSpecification;
import com.epam.lab.specification.SortSpecificationImpl;

import java.util.ArrayList;
import java.util.List;

public class SpecificationBuilder {
    private SpecificationBuilder() {
    }

    public static <T> List<SearchSpecification<T>> buildSearchSpecifications
            (List<SearchCriteria> searchCriteriaList, SearchSpecificationBuilderProducer<T> builderProducer) {
        List<SearchSpecification<T>> searchSpecifications = new ArrayList<>();
        for (SearchCriteria searchCriteria : searchCriteriaList) {
            QueryParameter queryParameter = searchCriteria.getQueryParameter();
            SearchSpecificationBuilder<T> searchSpecificationBuilder = builderProducer.get(queryParameter);
            SearchSpecification<T> searchSpecification = searchSpecificationBuilder.build(searchCriteria);
            searchSpecifications.add(searchSpecification);
        }
        return searchSpecifications;
    }

    public static <T> List<SortSpecification<T>> buildSortSpecifications(List<SortCriteria> sortCriteriaList) {
        List<SortSpecification<T>> sortSpecifications = new ArrayList<>();
        for (SortCriteria sortCriteria : sortCriteriaList) {
            QueryParameter queryParameter = sortCriteria.getQueryParameter();
            boolean ascending = sortCriteria.isAscending();
            SortSpecification<T> sortSpecification = new SortSpecificationImpl<>(queryParameter, ascending);
            sortSpecifications.add(sortSpecification);
        }
        return sortSpecifications;
    }
}

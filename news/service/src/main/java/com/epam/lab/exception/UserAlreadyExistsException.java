package com.epam.lab.exception;

public class UserAlreadyExistsException extends EntityAlreadyExistsException {
    public UserAlreadyExistsException(String entityData) {
        super("user", entityData);
    }
}

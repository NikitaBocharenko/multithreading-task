package com.epam.lab.exception;

public class EntityAlreadyExistsException extends RuntimeException {
    private final String entityData;

    public EntityAlreadyExistsException(String message, String entityData) {
        super(message);
        this.entityData = entityData;
    }

    public String getEntityData() {
        return entityData;
    }
}

package com.epam.lab.dto;

import com.epam.lab.model.Tag;
import com.epam.lab.validation.AddingValidation;
import com.epam.lab.validation.UpdatingValidation;

import javax.validation.constraints.*;
import java.util.Objects;

public class TagDto {
    @Null(message = "Id must be absent", groups = AddingValidation.class)
    @NotNull(message = "Id must be not null", groups = UpdatingValidation.class)
    @Positive(message = "Id must be positive", groups = UpdatingValidation.class)
    private Long id;

    @NotBlank(message = "Tag name must be not empty", groups = {AddingValidation.class, UpdatingValidation.class})
    @Size(max = 30, message = "Tag name must be no larger than 30 characters", groups = {AddingValidation.class, UpdatingValidation.class})
    private String name;

    public TagDto() {
    }

    public TagDto(Long id, String name) {
        this.setId(id);
        this.setName(name);
    }

    public TagDto(Tag tag) {
        this.setId(tag.getId());
        this.setName(tag.getName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagDto tagDTO = (TagDto) o;
        return Objects.equals(id, tagDTO.id) &&
                Objects.equals(name, tagDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "TagDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

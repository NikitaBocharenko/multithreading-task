package com.epam.lab.builder;

import com.epam.lab.dto.SearchCriteria;
import com.epam.lab.specification.DefaultStringSearchSpecification;
import com.epam.lab.specification.QueryParameter;
import com.epam.lab.specification.SearchSpecification;

public class DefaultStringSearchSpecificationBuilder<T> implements SearchSpecificationBuilder<T> {
    @Override
    public SearchSpecification<T> build(SearchCriteria searchCriteria) {
        QueryParameter queryParameter = searchCriteria.getQueryParameter();
        String value = searchCriteria.getValue();
        return new DefaultStringSearchSpecification<>(queryParameter, value);
    }
}

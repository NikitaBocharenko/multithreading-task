package com.epam.lab.folders;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class FoldersCreatorImplTest {
    private FoldersCreator foldersCreator;
    private Path rootPath;
    private final Integer expectedSubfoldersNumber = 9;

    private static final Logger LOG = Logger.getLogger(FoldersCreatorImplTest.class);

    @Before
    public void setUp() {
        String rootPathStr = "D:\\jsonRoot";
        rootPath = Paths.get(rootPathStr);
        foldersCreator = new FoldersCreatorImpl(expectedSubfoldersNumber, rootPathStr);
    }

    @Test
    public void shouldCreateFolders() throws IOException {
        Set<Path> foldersPaths = foldersCreator.createFolders();
        assertEquals(expectedSubfoldersNumber + 1, foldersPaths.size());
        long actualSubfoldersNumber = Files.find(
                rootPath,
                expectedSubfoldersNumber,
                (path, attributes) -> attributes.isDirectory()
        ).count() - 1;
        assertEquals(expectedSubfoldersNumber.intValue(), actualSubfoldersNumber);
    }

    @After
    public void cleanUp() {
        try (Stream<Path> walk = Files.walk(rootPath)) {
            walk.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .peek(file -> LOG.info("Deleting item: " + file))
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

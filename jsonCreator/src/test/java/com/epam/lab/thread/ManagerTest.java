package com.epam.lab.thread;

import com.epam.lab.data.JsonFactory;
import com.epam.lab.data.JsonFactoryImpl;
import com.epam.lab.folders.FoldersCreator;
import com.epam.lab.folders.FoldersCreatorImpl;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ManagerTest {
    private FileManager worker;
    private Path path;
    private final Integer expectedSubfoldersNumber = 9;

    private static final Logger LOG = Logger.getLogger(ManagerTest.class);

    @Before
    public void setUp() {
        String pathStr = "D:\\jsonRoot";
        path = Paths.get(pathStr);
        FoldersCreator foldersCreator = new FoldersCreatorImpl(expectedSubfoldersNumber, pathStr);
        JsonFactory jsonFactory = new JsonFactoryImpl();
        worker = new Manager(jsonFactory, foldersCreator, 80L, 1000, 10000L);
    }

    @Test
    public void shouldCreateFiles() throws InterruptedException, IOException {
        worker.startFilesCreation();
        while (worker.isFilesCreating()) {
            Thread.sleep(1000);
        }
        long expectedFilesNumber = worker.getCreatedFilesCount();
        long actualFilesNumber = Files.find(
                path,
                expectedSubfoldersNumber + 1,
                (path, attributes) -> attributes.isRegularFile()
        ).count();
        assertEquals(expectedFilesNumber, actualFilesNumber);
    }

    @After
    public void cleanUp() {
        LOG.info("Cleaning up...");
        try (Stream<Path> walk = Files.walk(path)) {
            walk.sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOG.info("Done");
    }
}

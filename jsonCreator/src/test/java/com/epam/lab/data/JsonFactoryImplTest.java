package com.epam.lab.data;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;

import java.io.StringReader;
import java.time.LocalDate;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class JsonFactoryImplTest {
    private final JsonFactory jsonFactory = new JsonFactoryImpl();

    @Test
    public void shouldCreateValidJsonString() {
        String jsonString = jsonFactory.createJsonValue(JsonValueType.VALID).getJsonString();
        JsonArray jsonArray = parseJsonStringToArray(jsonString);
        assertTrue(jsonArray.size() >= 3 && jsonArray.size() <= 10);
        for (int i = 0; i < jsonArray.size(); i++) {
            assertThatObjectValid(jsonArray.getJsonObject(i));
        }
    }

    @Test(expected = JsonParsingException.class)
    public void shouldCreateJsonStringWithInvalidFormat() {
        String jsonString = jsonFactory.createJsonValue(JsonValueType.INVALID_JSON_FORMAT).getJsonString();
        parseJsonStringToArray(jsonString);
    }

    @Test
    public void shouldCreateJsonStringWithInvalidFieldNames() {
        String jsonString = jsonFactory.createJsonValue(JsonValueType.INVALID_FIELD_NAMES).getJsonString();
        JsonArray jsonArray = parseJsonStringToArray(jsonString);
        JsonObject invalidObject = jsonArray.getJsonObject(jsonArray.size() / 2);
        assertFalse(invalidObject.containsKey(JsonFactory.TITLE_KEY));
        assertFalse(invalidObject.containsKey(JsonFactory.CREATION_DATE_KEY));
        assertFalse(invalidObject.containsKey(JsonFactory.AUTHOR_KEY));
    }

    @Test
    public void shouldCreateJsonStringWithInvalidValues() {
        String jsonString = jsonFactory.createJsonValue(JsonValueType.INVALID_VALUES).getJsonString();
        JsonArray jsonArray = parseJsonStringToArray(jsonString);
        JsonObject invalidObject = jsonArray.getJsonObject(jsonArray.size() / 2);
        String creationDateString = invalidObject.getString(JsonFactory.CREATION_DATE_KEY);
        LocalDate creationDate = LocalDate.parse(creationDateString);
        assertTrue(creationDate.isAfter(LocalDate.now()));
        String modificationDateString = invalidObject.getString(JsonFactory.MODIFICATION_DATE_KEY);
        LocalDate modificationDate = LocalDate.parse(modificationDateString);
        assertTrue(modificationDate.isAfter(LocalDate.now()));
    }

    @Test
    public void shouldCreateJsonStringWithDbConstraintsViolations() {
        String jsonString = jsonFactory.createJsonValue(JsonValueType.VIOLATES_DB_CONSTRAINTS).getJsonString();
        JsonArray jsonArray = parseJsonStringToArray(jsonString);
        JsonObject invalidObject = jsonArray.getJsonObject(jsonArray.size() / 2);
        String shortText = invalidObject.getString(JsonFactory.SHORT_TEXT_KEY);
        assertTrue(shortText.length() > 100);
    }

    private JsonArray parseJsonStringToArray(String jsonString) {
        JsonReader jsonReader = Json.createReader(new StringReader(jsonString));
        return jsonReader.readArray();
    }

    private void assertThatObjectValid(JsonObject jsonObject) {
        assertTrue(jsonObject.containsKey(JsonFactory.TITLE_KEY));
        String title = jsonObject.getString(JsonFactory.TITLE_KEY);
        assertTrue(title.length() <= 30);
        assertTrue(jsonObject.containsKey(JsonFactory.SHORT_TEXT_KEY));
        String shortText = jsonObject.getString(JsonFactory.SHORT_TEXT_KEY);
        assertTrue(shortText.length() <= 100);
        assertTrue(jsonObject.containsKey(JsonFactory.FULL_TEXT_KEY));
        String fullText = jsonObject.getString(JsonFactory.FULL_TEXT_KEY);
        assertTrue(fullText.length() <= 2000);
        assertTrue(jsonObject.containsKey(JsonFactory.CREATION_DATE_KEY));
        String creationDateString = jsonObject.getString(JsonFactory.CREATION_DATE_KEY);
        LocalDate creationDate = LocalDate.parse(creationDateString);
        assertTrue(creationDate.isBefore(LocalDate.now()) || creationDate.isEqual(LocalDate.now()));
        assertTrue(jsonObject.containsKey(JsonFactory.MODIFICATION_DATE_KEY));
        String modificationDateString = jsonObject.getString(JsonFactory.MODIFICATION_DATE_KEY);
        LocalDate modificationDate = LocalDate.parse(modificationDateString);
        assertTrue(modificationDate.isBefore(LocalDate.now()) || modificationDate.isEqual(LocalDate.now()));
        assertTrue(jsonObject.containsKey(JsonFactory.AUTHOR_KEY));
        assertTrue(jsonObject.getJsonObject(JsonFactory.AUTHOR_KEY).containsKey(JsonFactory.ID_KEY));
        int authorId = jsonObject.getJsonObject(JsonFactory.AUTHOR_KEY).getInt(JsonFactory.ID_KEY);
        assertEquals(1, authorId);
        assertTrue(jsonObject.containsKey(JsonFactory.TAGS_KEY));
        assertTrue(jsonObject.getJsonArray(JsonFactory.TAGS_KEY).getJsonObject(0).containsKey(JsonFactory.ID_KEY));
        int firstTagId = jsonObject.getJsonArray(JsonFactory.TAGS_KEY).getJsonObject(0).getInt(JsonFactory.ID_KEY);
        assertEquals(3, firstTagId);
        assertTrue(jsonObject.getJsonArray(JsonFactory.TAGS_KEY).getJsonObject(1).containsKey(JsonFactory.ID_KEY));
        int secondTagId = jsonObject.getJsonArray(JsonFactory.TAGS_KEY).getJsonObject(1).getInt(JsonFactory.ID_KEY);
        assertEquals(4, secondTagId);
        assertTrue(jsonObject.getJsonArray(JsonFactory.TAGS_KEY).getJsonObject(2).containsKey(JsonFactory.ID_KEY));
        int thirdTagId = jsonObject.getJsonArray(JsonFactory.TAGS_KEY).getJsonObject(2).getInt(JsonFactory.ID_KEY);
        assertEquals(6, thirdTagId);
    }
}

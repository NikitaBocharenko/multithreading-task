package com.epam.lab.exception;

public class BarrierException extends RuntimeException {
    public BarrierException(String message, Throwable cause) {
        super(message, cause);
    }
}

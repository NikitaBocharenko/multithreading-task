package com.epam.lab.exception;

public class FileCreatorException extends RuntimeException {
    public FileCreatorException(String message, Throwable cause) {
        super(message, cause);
    }
}

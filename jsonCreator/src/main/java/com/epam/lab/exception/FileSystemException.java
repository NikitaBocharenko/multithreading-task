package com.epam.lab.exception;

public class FileSystemException extends RuntimeException {
    public FileSystemException(String message, Throwable cause) {
        super(message, cause);
    }
}

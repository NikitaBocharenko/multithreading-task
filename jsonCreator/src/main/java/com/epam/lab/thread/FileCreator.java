package com.epam.lab.thread;

import com.epam.lab.data.JsonValue;
import com.epam.lab.data.JsonFactory;
import com.epam.lab.data.JsonValueType;
import com.epam.lab.exception.FileSystemException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;

public final class FileCreator implements Runnable {
    private final Path path;
    private final JsonFactory jsonFactory;
    private final ThreadManager threadManager;

    private static final String FILE_NAME_PREFIX = "file";
    private static final String FILE_EXTENSION = ".json";
    private static final Logger LOG = Logger.getLogger(FileCreator.class);

    public FileCreator(Path path, JsonFactory jsonFactory, ThreadManager threadManager) {
        this.path = path;
        this.jsonFactory = jsonFactory;
        this.threadManager = threadManager;
    }

    @Override
    public void run() {
        while (threadManager.isFileCreationShouldBeContinued()) {
            createFiles();
            threadManager.awaitOtherThreads();
        }
    }

    private void createFiles() {
        for (long fileNumber = threadManager.getNewFileNumber();
             threadManager.isNeedToCreateNewFile(fileNumber);
             fileNumber = threadManager.getNewFileNumber()) {
            JsonValue jsonValue = getJsonStringOfRequiredType(fileNumber);
            if (jsonValue.isValid()) {
                threadManager.addValidNewsNumber(jsonValue.getNewsNumber());
            }
            String filename = FILE_NAME_PREFIX + fileNumber + FILE_EXTENSION;
            Path filepath = path.resolve(filename);
            try (RandomAccessFile file = new RandomAccessFile(filepath.toString(), "rw");
                 FileChannel channel = file.getChannel();
                 FileLock lock = channel.lock()) {
                file.writeBytes(jsonValue.getJsonString());
            } catch (IOException e) {
                throw new FileSystemException("Problem with IO: " + e.getMessage(), e);
            }
            LOG.debug("File created: " + filepath);
        }
    }

    private JsonValue getJsonStringOfRequiredType(long fileNumber) {
        long remainder = fileNumber % 20;
        if (remainder < 16) {
            return jsonFactory.createJsonValue(JsonValueType.VALID);
        }
        if (remainder == 16) {
            return jsonFactory.createJsonValue(JsonValueType.INVALID_JSON_FORMAT);
        }
        if (remainder == 17) {
            return jsonFactory.createJsonValue(JsonValueType.INVALID_FIELD_NAMES);
        }
        if (remainder == 18) {
            return jsonFactory.createJsonValue(JsonValueType.INVALID_VALUES);
        }
        return jsonFactory.createJsonValue(JsonValueType.VIOLATES_DB_CONSTRAINTS);
    }
}

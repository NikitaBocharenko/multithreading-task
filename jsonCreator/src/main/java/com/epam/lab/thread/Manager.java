package com.epam.lab.thread;

import com.epam.lab.data.JsonFactory;
import com.epam.lab.exception.BarrierException;
import com.epam.lab.exception.FileCreatorException;
import com.epam.lab.folders.FoldersCreator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class Manager implements FileManager, ThreadManager {
    private CyclicBarrier barrier;
    private Long startTime;
    private Long iterationStartTime;
    private Long iterationCompletionTime;
    private final Long requiredDuration;
    private final Long sleepPeriod;
    private final Integer requiredFilesCount;
    private final AtomicLong fileCounterAtomic = new AtomicLong();
    private final AtomicLong validNewsNumberAtomic = new AtomicLong();
    private Integer cycleCount = 0;
    private final JsonFactory jsonFactory;
    private boolean isWorking = false;
    private final FoldersCreator foldersCreator;

    private static final Logger LOG = Logger.getLogger(Manager.class);

    public Manager(JsonFactory jsonFactory,
                   FoldersCreator foldersCreator,
                   @Value("${period-time}") Long sleepPeriod,
                   @Value("${files-count}") Integer requiredFilesCount,
                   @Value("${test-time}") Long requiredDuration) {
        this.jsonFactory = jsonFactory;
        this.sleepPeriod = sleepPeriod;
        this.requiredFilesCount = requiredFilesCount;
        this.requiredDuration = requiredDuration;
        this.foldersCreator = foldersCreator;
    }

    @Override
    public void startFilesCreation() {
        Set<Path> foldersPaths = this.foldersCreator.createFolders();
        LOG.info("Folders: " + foldersPaths);
        this.barrier = new CyclicBarrier(foldersPaths.size(), this::barrierLogic);
        List<Thread> threadList = new ArrayList<>(foldersPaths.size());
        for (Path folderPath : foldersPaths) {
            threadList.add(new Thread(new FileCreator(folderPath, jsonFactory, this)));
        }
        long currentTime = System.currentTimeMillis();
        this.startTime = currentTime;
        this.iterationStartTime = currentTime;
        this.iterationCompletionTime = 0L;
        threadList.forEach(Thread::start);
        isWorking = true;
        LOG.info("Work is started");
    }

    @Override
    public boolean isFilesCreating() {
        return isWorking;
    }

    @Override
    public long getCreatedFilesCount() {
        return fileCounterAtomic.get();
    }

    @Override
    public long getCreatedValidNewsCount() {
        return validNewsNumberAtomic.get();
    }

    private void barrierLogic() {
        iterationCompletionTime = System.currentTimeMillis();
        LOG.info("----------------------------------------------------");
        LOG.info(requiredFilesCount + " files have been created");
        LOG.info("Total files created: " + fileCounterAtomic.get());
        long lastIterationElapsedTime = iterationCompletionTime - iterationStartTime;
        LOG.info("Iteration elapsed time: " + lastIterationElapsedTime + " millis");
        long totalElapsedTime = iterationCompletionTime - startTime;
        LOG.info("Total elapsed time: " + totalElapsedTime + " millis");
        long remainingTime = requiredDuration - totalElapsedTime;
        LOG.info("Remaining time: " + (remainingTime > 0 ? remainingTime : 0) + " millis");
        if (remainingTime > 0) {
            this.cycleCount++;
            LOG.info("Sleeping for " + sleepPeriod + " millis before next iteration");
            sleep();
            this.iterationStartTime = System.currentTimeMillis();
            LOG.info("Next iteration started");
        } else {
            isWorking = false;
            LOG.info("Work is done");
        }
    }

    private void sleep() {
        try {
            Thread.sleep(sleepPeriod);
        } catch (InterruptedException e) {
            throw new BarrierException("Exception while barrier action was performed: " + e.getMessage(), e);
        }
    }

    @Override
    public boolean isFileCreationShouldBeContinued() {
        return requiredDuration > (iterationCompletionTime - startTime);
    }

    @Override
    public void awaitOtherThreads() {
        fileCounterAtomic.getAndDecrement();
        try {
            this.barrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            throw new FileCreatorException("Exception in awaiting at barrier: " + e.getMessage(), e);
        }
    }

    @Override
    public long getNewFileNumber() {
        return fileCounterAtomic.getAndIncrement();
    }

    @Override
    public boolean isNeedToCreateNewFile(long fileNumber) {
        return fileNumber - requiredFilesCount * cycleCount < requiredFilesCount;
    }

    @Override
    public void addValidNewsNumber(long newsNumber) {
        validNewsNumberAtomic.getAndAdd(newsNumber);
    }
}

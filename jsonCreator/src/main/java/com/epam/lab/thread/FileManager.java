package com.epam.lab.thread;

public interface FileManager {
    void startFilesCreation();

    boolean isFilesCreating();

    long getCreatedFilesCount();

    long getCreatedValidNewsCount();
}

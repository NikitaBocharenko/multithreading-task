package com.epam.lab.thread;

public interface ThreadManager {
    boolean isFileCreationShouldBeContinued();

    void awaitOtherThreads();

    long getNewFileNumber();

    boolean isNeedToCreateNewFile(long fileNumber);

    void addValidNewsNumber(long newsNumber);
}

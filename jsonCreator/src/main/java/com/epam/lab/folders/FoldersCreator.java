package com.epam.lab.folders;

import java.nio.file.Path;
import java.util.Set;

public interface FoldersCreator {
    Set<Path> createFolders();
}

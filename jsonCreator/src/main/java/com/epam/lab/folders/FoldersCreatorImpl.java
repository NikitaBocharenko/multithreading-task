package com.epam.lab.folders;

import com.epam.lab.exception.FileSystemException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FoldersCreatorImpl implements FoldersCreator {
    private final Integer subfoldersCount;
    private final Integer averageDepth;
    private final Path rootPath;

    private static final Integer SUBFOLDERS_DEPTH_COEFFICIENT = 3;
    private static final String FOLDER_NAME_PREFIX = "folder";
    private static final Logger LOG = Logger.getLogger(FoldersCreatorImpl.class);

    public FoldersCreatorImpl(@Value("${subfolders-count}") Integer subfoldersCount,
                              @Value("${root-folder}") String rootPathStr) {
        this.subfoldersCount = subfoldersCount;
        this.averageDepth = subfoldersCount / SUBFOLDERS_DEPTH_COEFFICIENT;
        this.rootPath = Paths.get(rootPathStr);
    }

    @Override
    public Set<Path> createFolders() {
        LOG.debug("Required subfolders count: " + subfoldersCount);
        LOG.debug("Average depth: " + averageDepth);
        LOG.debug("Root path: " + rootPath);
        try {
            if (Files.isDirectory(rootPath)) {
                return getExistingFolders();
            }
            return createNewFolders();
        } catch (IOException e) {
            throw new FileSystemException("Problem with IO: " + e.getMessage(), e);
        }
    }

    private Set<Path> getExistingFolders() throws IOException {
        return Files.walk(rootPath)
                .filter(path -> Files.isDirectory(path))
                .filter(path -> !path.endsWith("error"))
                .collect(Collectors.toSet());
    }

    private Set<Path> createNewFolders() throws IOException {
        Set<Path> foldersPaths = new HashSet<>();
        Files.createDirectory(rootPath);
        foldersPaths.add(rootPath);
        logCurrentSubfoldersCount();
        while (countAllSubfolders() < subfoldersCount) {
            Path currentFolderPath = rootPath;
            int requiredSubfolderLevel = getRandomSubfolderLevel();
            LOG.debug("New subfolder level: " + requiredSubfolderLevel);
            while (requiredSubfolderLevel >= 0 && countAllSubfolders() < subfoldersCount) {
                currentFolderPath = getOrCreateSubdirectory(currentFolderPath, requiredSubfolderLevel);
                foldersPaths.add(currentFolderPath);
                logCurrentSubfoldersCount();
                requiredSubfolderLevel--;
            }
        }
        return foldersPaths;
    }

    private Path getOrCreateSubdirectory(Path path, int requiredSubfolderLevel) throws IOException {
        long currentFolderSubfoldersCount = countSubfolders(path);
        if (currentFolderSubfoldersCount > 0 && requiredSubfolderLevel > 0) {
            return getSubdirectory(path, currentFolderSubfoldersCount);
        }
        return createSubdirectory(path, currentFolderSubfoldersCount);
    }

    private Path getSubdirectory(Path path, long subfoldersCount) {
        int subfolderNumber = (int) (Math.random() * subfoldersCount);
        Path foundSubfolderPath = path.resolve(FOLDER_NAME_PREFIX + subfolderNumber);
        LOG.debug("Subfolder found: " + foundSubfolderPath);
        return foundSubfolderPath;
    }

    private Path createSubdirectory(Path path, long subfoldersCount) throws IOException {
        Path subfolderPath = path.resolve(FOLDER_NAME_PREFIX + subfoldersCount);
        LOG.debug("Subfolder created: " + subfolderPath);
        return Files.createDirectory(subfolderPath);
    }

    private long countSubfolders(Path rootPath) throws IOException {
        return Files.find(
                rootPath,
                1,
                (path, attributes) -> attributes.isDirectory()
        ).count() - 1;
    }

    private long countAllSubfolders() throws IOException {
        return Files.find(
                rootPath,
                averageDepth * 2 + 1,
                (path, attributes) -> attributes.isDirectory()
        ).count() - 1;
    }

    private int getRandomSubfolderLevel() {
        return (int) (Math.random() * averageDepth * 2);
    }

    private void logCurrentSubfoldersCount() throws IOException {
        LOG.debug("Current folders count: " + countAllSubfolders());
    }
}

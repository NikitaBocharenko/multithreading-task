package com.epam.lab.db;

import com.epam.lab.exception.DatabaseException;
import com.epam.lab.exception.FileSystemException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Component
public class WorkCheckerImpl implements WorkChecker {
    private final JdbcTemplate jdbcTemplate;
    private final Path errorPath;

    private static final String ERROR_FOLDER_NAME = "error";

    public WorkCheckerImpl(DataSource dataSource,
                           @Value("${root-folder}") String rootPathStr) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.errorPath = Paths.get(rootPathStr, ERROR_FOLDER_NAME);
    }

    @Override
    public boolean checkErrorFilesNumber(long expectedErrorFilesNumber) {
        long actualErrorFilesNumber = countActualErrorFiles();
        return expectedErrorFilesNumber == actualErrorFilesNumber;
    }

    private long countActualErrorFiles() {
        try {
            return Files.find(
                    errorPath,
                    1,
                    (path, attributes) -> attributes.isRegularFile()
            ).count();
        } catch (IOException e) {
            throw new FileSystemException("IO exception", e);
        }
    }

    @Override
    public boolean checkValidNewsNumber(long expectedValidNewsNumber) {
        Long actualValidNewsNumber = jdbcTemplate.queryForObject("select count(*) from nm.news", Long.class);
        if (Objects.isNull(actualValidNewsNumber)) {
            throw new DatabaseException("Query for counting news returns NULL");
        }
        return actualValidNewsNumber == expectedValidNewsNumber;
    }
}

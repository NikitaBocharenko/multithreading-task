package com.epam.lab.db;

public interface WorkChecker {
    boolean checkErrorFilesNumber(long expectedErrorFilesNumber);
    boolean checkValidNewsNumber(long expectedValidNewsNumber);
}

package com.epam.lab.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@ComponentScan({"com.epam.lab.data","com.epam.lab.folders","com.epam.lab.thread","com.epam.lab.db"})
@PropertySource("classpath:app.properties")
public class SpringConfig {
    private static final String CONNECTION_POOL_PROPERTIES_NAME = "/database.properties";

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig(CONNECTION_POOL_PROPERTIES_NAME);
        return new HikariDataSource(hikariConfig);
    }
}

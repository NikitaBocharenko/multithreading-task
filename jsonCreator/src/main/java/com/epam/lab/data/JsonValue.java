package com.epam.lab.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class JsonValue {
    private final String jsonString;
    private final boolean isValid;
    private final Integer newsNumber;
}

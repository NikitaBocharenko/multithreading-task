package com.epam.lab.data;

import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class JsonFactoryImpl implements JsonFactory {
    private final Faker faker = new Faker();
    private static final String TITLE_PREFIX = "Title ";
    private final AtomicLong atomicTitleNumber = new AtomicLong();

    @Override
    public JsonValue createJsonValue(JsonValueType jsonValueType) {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        int arraySize = faker.number().numberBetween(3, 10);
        for (int i = 0; i < arraySize; i++) {
            JsonObject jsonObject =
                    i == arraySize / 2 ?
                            createSpecificObject(jsonValueType) :
                            createValidObject();
            jsonArrayBuilder.add(jsonObject);
        }
        boolean isJsonValid = jsonValueType.equals(JsonValueType.VALID);
        JsonArray jsonArray = jsonArrayBuilder.build();
        String jsonString = jsonArray.toString();
        if (jsonValueType.equals(JsonValueType.INVALID_JSON_FORMAT)) {
            jsonString = jsonString.substring(5);
        }
        return new JsonValue(jsonString, isJsonValid, arraySize);
    }

    private JsonObject createSpecificObject(JsonValueType jsonValueType) {
        switch (jsonValueType) {
            case VALID:
            case INVALID_JSON_FORMAT:
                return createValidObject();
            case INVALID_FIELD_NAMES:
                return createObjectWithInvalidFieldNames();
            case INVALID_VALUES:
                return createObjectWithInvalidValues();
            case VIOLATES_DB_CONSTRAINTS:
                return createObjectWithDbConstraintsViolationValues();
            default:
                throw new IllegalArgumentException("JsonType: " + jsonValueType + " is illegal in this switch");
        }
    }

    private String getValidDate() {
        return LocalDate.now().minusDays(faker.random().nextInt(20, 100)).format(DateTimeFormatter.ISO_DATE);
    }

    private String getInvalidDate() {
        return LocalDate.now().plusDays(faker.random().nextInt(20, 100)).format(DateTimeFormatter.ISO_DATE);
    }

    private JsonObject createValidObject() {
        return Json.createObjectBuilder()
                .add(TITLE_KEY, TITLE_PREFIX + atomicTitleNumber.getAndIncrement())
                .add(SHORT_TEXT_KEY, faker.lorem().sentence(3, 2))
                .add(FULL_TEXT_KEY, faker.lorem().sentence(100, 20))
                .add(CREATION_DATE_KEY, getValidDate())
                .add(MODIFICATION_DATE_KEY, getValidDate())
                .add(AUTHOR_KEY, Json.createObjectBuilder()
                        .add(ID_KEY, 1))
                .add(TAGS_KEY, Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 3))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 4))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 6))
                ).build();
    }

    private JsonObject createObjectWithInvalidFieldNames() {
        return Json.createObjectBuilder()
                .add(faker.lorem().word(), TITLE_PREFIX + atomicTitleNumber.getAndIncrement())
                .add(SHORT_TEXT_KEY, faker.lorem().sentence(3, 2))
                .add(FULL_TEXT_KEY, faker.lorem().sentence(100, 20))
                .add(faker.lorem().word(), getValidDate())
                .add(MODIFICATION_DATE_KEY, getValidDate())
                .add(faker.lorem().word(), Json.createObjectBuilder()
                        .add(ID_KEY, 1))
                .add(TAGS_KEY, Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 3))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 4))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 6))
                ).build();
    }

    private JsonObject createObjectWithInvalidValues() {
        return Json.createObjectBuilder()
                .add(TITLE_KEY, TITLE_PREFIX + atomicTitleNumber.getAndIncrement())
                .add(SHORT_TEXT_KEY, faker.lorem().sentence(3, 2))
                .add(FULL_TEXT_KEY, faker.lorem().sentence(100, 20))
                .add(CREATION_DATE_KEY, getInvalidDate())
                .add(MODIFICATION_DATE_KEY, getInvalidDate())
                .add(AUTHOR_KEY, Json.createObjectBuilder()
                        .add(ID_KEY, 1))
                .add(TAGS_KEY, Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 3))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 4))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 6))
                ).build();
    }

    private JsonObject createObjectWithDbConstraintsViolationValues() {
        return Json.createObjectBuilder()
                .add(TITLE_KEY, TITLE_PREFIX + atomicTitleNumber.getAndIncrement())
                .add(SHORT_TEXT_KEY, faker.lorem().sentence(50))
                .add(FULL_TEXT_KEY, faker.lorem().sentence(100, 20))
                .add(CREATION_DATE_KEY, getValidDate())
                .add(MODIFICATION_DATE_KEY, getValidDate())
                .add(AUTHOR_KEY, Json.createObjectBuilder()
                        .add(ID_KEY, 1))
                .add(TAGS_KEY, Json.createArrayBuilder()
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 3))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 4))
                        .add(Json.createObjectBuilder()
                                .add(ID_KEY, 6))
                ).build();
    }
}

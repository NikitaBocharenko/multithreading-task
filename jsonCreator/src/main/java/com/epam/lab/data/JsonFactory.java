package com.epam.lab.data;

public interface JsonFactory {
    String TITLE_KEY = "title";
    String SHORT_TEXT_KEY = "shortText";
    String FULL_TEXT_KEY = "fullText";
    String CREATION_DATE_KEY = "creationDate";
    String MODIFICATION_DATE_KEY = "modificationDate";
    String AUTHOR_KEY = "authorDto";
    String TAGS_KEY = "tagDtoSet";
    String ID_KEY = "id";

    JsonValue createJsonValue(JsonValueType type);
}

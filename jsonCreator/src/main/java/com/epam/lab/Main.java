package com.epam.lab;

import com.epam.lab.config.SpringConfig;
import com.epam.lab.db.WorkChecker;
import com.epam.lab.thread.FileManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class);
    private static final Integer MAX_ERROR_FILES_CHECK_NUMBER = 10;
    private static final Integer MAX_VALID_NEWS_CHECK_NUMBER = 5;
    private static final Long ERROR_FILES_CHECK_SLEEP_PERIOD = 6000L;
    private static final Long VALID_NEWS_CHECK_SLEEP_PERIOD = 1000L;

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        FileManager fileManager = context.getBean(FileManager.class);
        fileManager.startFilesCreation();
        do {
            Thread.sleep(1000);
        } while (fileManager.isFilesCreating());

        long errorFilesNumber = (long) (fileManager.getCreatedFilesCount() * 0.2);
        LOG.info("Error files number: " + errorFilesNumber);
        WorkChecker workChecker = context.getBean(WorkChecker.class);
        if (checkErrorFiles(workChecker, errorFilesNumber)) {
            LOG.info("Error files number is correct");
        } else {
            LOG.info("Error files number is wrong!");
            LOG.info("Reading was done incorrectly!");
            return;
        }

        long validNewsNumber = fileManager.getCreatedValidNewsCount();
        LOG.info("Valid news number: " + validNewsNumber);
        if (checkValidNews(workChecker, validNewsNumber)) {
            LOG.info("Valid news number in database is correct");
            LOG.info("Reading was done correctly");
        } else {
            LOG.info("Valid news number in database is wrong!");
            LOG.info("Reading was done incorrectly!");
        }
    }

    private static boolean checkErrorFiles(WorkChecker workChecker, long errorFilesNumber) throws InterruptedException {
        int currentCheckTry = 0;
        while(!workChecker.checkErrorFilesNumber(errorFilesNumber) && currentCheckTry++ < MAX_ERROR_FILES_CHECK_NUMBER) {
            LOG.info("Error files number doesn't match yet, waiting...");
            Thread.sleep(ERROR_FILES_CHECK_SLEEP_PERIOD);
        }
        return currentCheckTry < MAX_ERROR_FILES_CHECK_NUMBER;
    }

    private static boolean checkValidNews(WorkChecker workChecker, long validNewsNumber) throws InterruptedException {
        int currentCheckTry = 0;
        while(!workChecker.checkValidNewsNumber(validNewsNumber) && currentCheckTry++ < MAX_VALID_NEWS_CHECK_NUMBER) {
            LOG.info("Valid news number in database doesn't match yet, waiting...");
            Thread.sleep(VALID_NEWS_CHECK_SLEEP_PERIOD);
        }
        return currentCheckTry < MAX_VALID_NEWS_CHECK_NUMBER;
    }
}
